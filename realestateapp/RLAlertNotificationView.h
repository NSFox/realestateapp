//
//  RLAlertNotificationView.h
//  realestateapp
//
//  Created by Юрий Дурнев on 24.08.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RLAlertNotificationView : NSObject

+ (RLAlertNotificationView *)sharedClient;

- (void)addedMessageTitle:(NSString *)title message:(NSString *)message onView:(UIViewController *)viewController;
- (void)addedErrorMessageTitle:(NSString *)title message:(NSString *)message onView:(UIViewController *)viewController;
@end
