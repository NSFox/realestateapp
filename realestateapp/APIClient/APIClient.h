//
//  APIClient.h
//  realestateapp
//
//  Created by Юрий Дурнев on 30/03/15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^successBlock)(NSURLSessionDataTask *task, id responseObject);
typedef void (^failBlock)(NSURLSessionDataTask *task, NSError *error);

@interface APIClient : AFHTTPSessionManager

@property (nonatomic, copy) successBlock successBlock;
@property (nonatomic, copy) failBlock failBlock;

+ (APIClient *)sharedClient;

//LogIn User
- (void)logInWithUserName:(NSString *)email
              andPassword:(NSString *)password
         withSuccessBlock:successBlock
             andFailBlock:failBlock;

//Register new Users
- (void)registerUserWithName:(NSString *)email
                 andPassword:(NSString *)password
            withSuccessBlock:successBlock
                andFailBlock:failBlock;

//Restoration Password
- (void)restorationPasswordWithMail:(NSString *)email
                   withSuccessBlock:successBlock
                       andFailBlock:failBlock;

//Get Map Items
- (void)getAdvertOnMapCoordinats:(NSDictionary *)parametr
                withSuccessBlock:successBlock
                    andFailBlock:failBlock;

//Get Adversement on ID
- (void)getAdversementOnID:(NSString *)adversementID
          withSuccessBlock:successBlock
              andFailBlock:failBlock;

//Get View Count on ID
- (void)getViewCountOnID:(NSString *)adversementID
        withSuccessBlock:successBlock
            andFailBlock:failBlock;

//SaveNewDataUser
- (void)saveNewDataUserWithParams:(NSDictionary *)params
                 withSuccessBlock:success
                     andFailBlock:failBlock;

//Получение рубрикатора сайта (для навигации по листингам)
- (void)getCategoryWithParams:(NSDictionary *)params
             withSuccessBlock:successBlock
                 andFailBlock:failBlock;

//Получение списка регионов
- (void)getRegionWithSuccessBlock:successBlock
                     andFailBlock:failBlock;

//Получение списка городов
- (void)getCityWithParams:(NSDictionary *)params
         withSuccessBlock:successBlock
             andFailBlock:failBlock;

//Получаем локацию по IP :)
- (void)getLocationWithSuccessBlock:successBlock
                       andFailBlock:failBlock;

//Получение полей для подачи объявления
- (void)getPostAdvertWithParams:(NSDictionary *)params
               withSuccessBlock:successBlock
                   andFailBlock:failBlock;

//Подача объявления
- (void)postAdvertWithParams:(NSDictionary *)params
                       image:(NSArray *)imageArray
            withSuccessBlock:successBlock
                andFailBlock:failBlock;

//Получение списка своих объявлений
- (void)getMyAdvertWithSuccessBlock:successBlock
                       andFailBlock:failBlock;

//Получение списка избранных объявлений
- (void)getFavoriteAdvertWithSuccessBlock:successBlock
                             andFailBlock:failBlock;

//Добвление объявления в список избранных
- (void)postAddedAdvertToFavoriteonID:(NSString *)adversementID
                     withSuccessBlock:successBlock
                         andFailBlock:failBlock;

//Удаление объявления из избранного
- (void)deleteAdertOnFavorite:(NSString *)adversementID
             withSuccessBlock:successBlock
                 andFailBlock:failBlock;

//Деактивация объявления
- (void)deactiveAdvert:(NSString *)adversementID
      withSuccessBlock:successBlock
          andFailBlock:failBlock;

//удаление объявления
- (void)deleteAdvert:(NSString *)adversementID
    withSuccessBlock:successBlock
        andFailBlock:failBlock;

//Отправка жалобы на объявление
- (void)sendReasonOnAdvert:(NSString *)adversementID
                    reason:(NSString *)reason
          withSuccessBlock:successBlock
              andFailBlock:failBlock;

//Получеие списка фильтров
- (void)getFilters:(NSDictionary *)params
  withSuccessBlock:successBlock
      andFailBlock:failBlock;

@end
