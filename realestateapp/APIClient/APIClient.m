//
//  APIClient.m
//  realestateapp
//
//  Created by Юрий Дурнев on 30/03/15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "APIClient.h"
static NSString * const versionAPI = @"1.2/";
static NSString * const baseURL = @"http://irr.ru";


@implementation APIClient

+ (APIClient *)sharedClient {
    static APIClient *sharedHTTPClient = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedHTTPClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:baseURL]];
    });
    
    return sharedHTTPClient;
}

- (instancetype)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    
    if (self) {
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        self.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        
    }
    return self;
}

//LogIn User
- (void)logInWithUserName:(NSString *)email
              andPassword:(NSString *)password
         withSuccessBlock:successBlock
             andFailBlock:failBlock {
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"username"] = email;
    parameters[@"password"] = password;
    [self POST:@"/mobile_api/1.2/account/login" parameters:parameters success:successBlock failure:failBlock];
}

//Register new Users
- (void)registerUserWithName:(NSString *)email
              andPassword:(NSString *)password
         withSuccessBlock:successBlock
             andFailBlock:failBlock {
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"username"] = email;
    parameters[@"password"] = password;
    [self POST:@"/mobile_api/1.2/account" parameters:parameters success:successBlock failure:failBlock];
}

//Restoration Password
- (void)restorationPasswordWithMail:(NSString *)email
            withSuccessBlock:successBlock
                andFailBlock:failBlock {
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"username"] = email;
    [self POST:@"/mobile_api/1.2/account/restore" parameters:parameters success:successBlock failure:failBlock];
}

//SaveNewDataUser
- (void)saveNewDataUserWithParams:(NSDictionary *)params
                withSuccessBlock:successBlock
                    andFailBlock:failBlock {
    [self PUT:@"/mobile_api/1.2/account" parameters:params success:successBlock failure:failBlock];
}

//Get Map Items
- (void)getAdvertOnMapCoordinats:(NSDictionary *)parametr
                withSuccessBlock:success
                    andFailBlock:failBlock {
    [self GET:@"/mobile_api/1.2/advertisements/map" parameters:parametr success:success failure:failBlock];
}

//Get Adversement on ID
- (void)getAdversementOnID:(NSString *)adversementID
    withSuccessBlock:successBlock
        andFailBlock:failBlock {
    NSString *urlString = [NSString stringWithFormat:@"/mobile_api/1.2/advertisements/advert/%@", adversementID];
    [self GET:urlString parameters:@{@"auth_token": [self getToken] != nil ? [self getToken]: @""} success:successBlock failure:failBlock];
}

//Получение рубрикатора сайта (для навигации по листингам)
- (void)getCategoryWithParams:(NSDictionary *)params
             withSuccessBlock:successBlock
                 andFailBlock:failBlock {
    [self GET:@"/mobile_api/1.2/categories" parameters:params success:successBlock failure:failBlock];
}

//Получение списка регионов
- (void)getRegionWithSuccessBlock:successBlock
                     andFailBlock:failBlock {
    [self GET:@"/mobile_api/1.2/regions" parameters:nil success:successBlock failure:failBlock];
}

//Получаем локацию по IP :)
- (void)getLocationWithSuccessBlock:successBlock
                     andFailBlock:failBlock {
    [self GET:@"/mobile_api/1.2/iplocation" parameters:nil success:successBlock failure:failBlock];
}

//Получение списка городов
- (void)getCityWithParams:(NSDictionary *)params
           withSuccessBlock:successBlock
               andFailBlock:failBlock {
    [self GET:@"/mobile_api/1.2/regions/cities" parameters:params success:successBlock failure:failBlock];
}

//Получение полей для подачи объявления
- (void)getPostAdvertWithParams:(NSDictionary *)params
         withSuccessBlock:successBlock
             andFailBlock:failBlock {
    [self GET:@"/mobile_api/1.2/categories/fields/post" parameters:params success:successBlock failure:failBlock];
}

//Подача объявления
- (void)postAdvertWithParams:(NSDictionary *)params
                       image:(NSArray *)imageArray
            withSuccessBlock:successBlock
                andFailBlock:failBlock {
    [self POST:@"/mobile_api/1.2/advertisements/advert" parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        if (imageArray != 0 ) {
            for (NSUInteger i = 0; i < imageArray.count; i++) {
                UIImage *image = imageArray[i];
                NSData *imageToUpload = UIImageJPEGRepresentation(image, 1.0);
                NSString *nameImage = [NSString stringWithFormat:@"image%lu", (unsigned long)i];
                NSString *nameForServer = [NSString stringWithFormat:@"%@.jpg", nameImage];
                [formData appendPartWithFileData:imageToUpload name:nameImage fileName:nameForServer mimeType:@"image/jpeg"];
            }
        }
    } success:successBlock failure:failBlock];
}


//Получеие списка фильтров
- (void)getFilters:(NSDictionary *)params
  withSuccessBlock:successBlock
      andFailBlock:failBlock {
    [self GET:@"/mobile_api/1.2/categories/fields/search" parameters:params success:successBlock failure:failBlock];
}

//Get View Count on ID
- (void)getViewCountOnID:(NSString *)adversementID
          withSuccessBlock:successBlock
              andFailBlock:failBlock {
    NSString *urlString = [NSString stringWithFormat:@"/mobile_api/1.2/advertisements/advert/%@/view_history", adversementID];
    [self GET:urlString parameters:nil success:successBlock failure:failBlock];
}

//Получение списка своих объявлений
- (void)getMyAdvertWithSuccessBlock:successBlock
                       andFailBlock:failBlock {
    [self GET:@"/mobile_api/1.2/advertisements/personal" parameters:@{@"auth_token": [self getToken] != nil ? [self getToken]: @""} success:successBlock failure:failBlock];
}

//Получение списка избранных объявлений
- (void)getFavoriteAdvertWithSuccessBlock:successBlock
                             andFailBlock:failBlock {

    [self GET:@"/mobile_api/1.2/advertisements/favorites" parameters:@{@"auth_token": [self getToken] != nil ? [self getToken]: @""} success:successBlock failure:failBlock];
}

//Добвление объявления в список избранных
- (void)postAddedAdvertToFavoriteonID:(NSString *)adversementID
                     withSuccessBlock:successBlock
                         andFailBlock:failBlock {
    NSString *URLString = [NSString stringWithFormat:@"/mobile_api/1.2/advertisements/advert/%@/favorite", adversementID];
    [self POST:URLString parameters:@{@"auth_token": [self getToken] != nil ? [self getToken]: @""} success:successBlock  failure:failBlock];
}

//Удаление объявления из избранного
- (void)deleteAdertOnFavorite:(NSString *)adversementID
             withSuccessBlock:successBlock
                 andFailBlock:failBlock {
    NSString *URLString = [NSString stringWithFormat:@"/mobile_api/1.2/advertisements/advert/%@/favorite", adversementID];
    [self DELETE:URLString parameters:@{@"auth_token": [self getToken] != nil ? [self getToken]: @""}  success:successBlock failure:failBlock];
}

//Деактивация объявления
- (void)deactiveAdvert:(NSString *)adversementID
      withSuccessBlock:successBlock
          andFailBlock:failBlock {
    NSString *URLString = [NSString stringWithFormat:@"/mobile_api/1.2/advertisements/advert/%@/deactivate", adversementID];
    [self POST:URLString parameters:@{@"auth_token": [self getToken] != nil ? [self getToken]: @""} success:successBlock  failure:failBlock];
}

//удаление объявления
- (void)deleteAdvert:(NSString *)adversementID
    withSuccessBlock:successBlock
        andFailBlock:failBlock {
    NSString *URLString = [NSString stringWithFormat:@"/mobile_api/1.2/advertisements/advert/%@", adversementID];
    [self DELETE:URLString parameters:@{@"auth_token": [self getToken] != nil ? [self getToken]: @""}  success:successBlock failure:failBlock];
}

//Отправка жалобы на объявление
- (void)sendReasonOnAdvert:(NSString *)adversementID
                   reason:(NSString *)reason
          withSuccessBlock:successBlock
              andFailBlock:failBlock {
    NSString *URLString = [NSString stringWithFormat:@"/mobile_api/1.2/advertisements/advert/%@/complain", adversementID];
    [self POST:URLString parameters:@{@"reason": reason}  success:successBlock failure:failBlock];
}

- (NSString *)getToken {
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
}

@end
