//
//  LoginModel.m
//  realestateapp
//
//  Created by Юрий Дурнев on 27.07.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "LoginModel.h"

@implementation LoginModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"userMail": @"email",
             @"userFirstName": @"first_name",
             @"userLastName": @"last_name",
             @"userPhone": @"mobile",
             @"userSkype": @"skype"};
}

+ (LoginModel *)deserializeAppInfosFromJSON:(NSDictionary *)appInfosJSON {
    NSError *error;
    
    LoginModel *arrayModel = [MTLJSONAdapter modelOfClass:[LoginModel class] fromJSONDictionary:appInfosJSON error:&error];
    if (error) {
        NSLog(@"Couldn't convert app infos JSON to MAdvert models: %@", error);
        return nil;
    }
    
    return arrayModel;
}

@end
