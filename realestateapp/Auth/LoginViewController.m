//
//  LoginViewController.m
//  realestateapp
//
//  Created by Юрий Дурнев on 25/03/15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "LoginViewController.h"
#import "APIClient.h"
#import "LoginModel.h"

@interface LoginViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *emailTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *logInButton;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelRegisterButton;
@property (weak, nonatomic) IBOutlet UIButton *restorationButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelRestorationButton;
@property (weak, nonatomic) IBOutlet UITextField *registrationTextField;
@property (weak, nonatomic) IBOutlet UIView *registrationView;
@property (weak, nonatomic) IBOutlet UIView *restorationPasswordView;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *registerEmailTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *registerPasswordTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *restorationPassword;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.registrationView.alpha = 0;
    self.restorationPasswordView.alpha = 0;
    [self setUpLayer];
}

- (void)setUpLayer {
    self.emailTextField.layer.masksToBounds = YES;
    self.emailTextField.layer.borderWidth = 1.;
    self.emailTextField.layer.borderColor = [UIColor colorWithRed:0.455 green:0.471 blue:0.494 alpha:1.000].CGColor;
    self.emailTextField.layer.cornerRadius = 5.;
    
    self.passwordTextField.layer.masksToBounds = self.emailTextField.layer.masksToBounds;
    self.passwordTextField.layer.borderWidth = self.emailTextField.layer.borderWidth;
    self.passwordTextField.layer.borderColor = self.emailTextField.layer.borderColor;
    self.passwordTextField.layer.cornerRadius = self.emailTextField.layer.cornerRadius;
    
    self.registerEmailTextField.layer.masksToBounds = self.emailTextField.layer.masksToBounds;
    self.registerEmailTextField.layer.borderWidth = self.emailTextField.layer.borderWidth;
    self.registerEmailTextField.layer.borderColor = self.emailTextField.layer.borderColor;
    self.registerEmailTextField.layer.cornerRadius = self.emailTextField.layer.cornerRadius;
    
    self.registerPasswordTextField.layer.masksToBounds = self.emailTextField.layer.masksToBounds;
    self.registerPasswordTextField.layer.borderWidth = self.emailTextField.layer.borderWidth;
    self.registerPasswordTextField.layer.borderColor = self.emailTextField.layer.borderColor;
    self.registerPasswordTextField.layer.cornerRadius = self.emailTextField.layer.cornerRadius;
    
    self.restorationPassword.layer.masksToBounds = self.emailTextField.layer.masksToBounds;
    self.restorationPassword.layer.borderWidth = self.emailTextField.layer.borderWidth;
    self.restorationPassword.layer.borderColor = self.emailTextField.layer.borderColor;
    self.restorationPassword.layer.cornerRadius = self.emailTextField.layer.cornerRadius;
    
    self.logInButton.layer.masksToBounds = YES;
    self.logInButton.layer.borderWidth = 1.;
    self.logInButton.layer.borderColor = [UIColor colorWithRed:0.965 green:0.294 blue:0.318 alpha:1.000].CGColor;
    self.logInButton.layer.cornerRadius = 5.;
    
    self.registerButton.layer.masksToBounds = self.logInButton.layer.masksToBounds;
    self.registerButton.layer.borderWidth = self.logInButton.layer.borderWidth;
    self.registerButton.layer.borderColor = self.logInButton.layer.borderColor;
    self.registerButton.layer.cornerRadius = self.logInButton.layer.cornerRadius;
    
    self.cancelRegisterButton.layer.masksToBounds = self.logInButton.layer.masksToBounds;
    self.cancelRegisterButton.layer.borderWidth = self.logInButton.layer.borderWidth;
    self.cancelRegisterButton.layer.borderColor = self.logInButton.layer.borderColor;
    self.cancelRegisterButton.layer.cornerRadius = self.logInButton.layer.cornerRadius;
    
    self.restorationButton.layer.masksToBounds = self.logInButton.layer.masksToBounds;
    self.restorationButton.layer.borderWidth = self.logInButton.layer.borderWidth;
    self.restorationButton.layer.borderColor = self.logInButton.layer.borderColor;
    self.restorationButton.layer.cornerRadius = self.logInButton.layer.cornerRadius;
    
    self.cancelRestorationButton.layer.masksToBounds = self.logInButton.layer.masksToBounds;
    self.cancelRestorationButton.layer.borderWidth = self.logInButton.layer.borderWidth;
    self.cancelRestorationButton.layer.borderColor = self.logInButton.layer.borderColor;
    self.cancelRestorationButton.layer.cornerRadius = self.logInButton.layer.cornerRadius;
}

#pragma mark - IBAction

- (IBAction)forgotPassword:(UIButton *)sender {
    
    [UIView animateWithDuration:0.3 animations:^{
        self.registrationView.alpha = 0;
        self.restorationPasswordView.alpha = 1;
    }];
}

- (IBAction)logIn:(UIButton *)sender {
    NSString *email = self.emailTextField.text;
    NSString *password = self.passwordTextField.text;

    APIClient *client = [APIClient sharedClient];
    [client logInWithUserName:email andPassword:password withSuccessBlock:^(NSURLSessionDataTask *task, id responseObject) {
        [self errorServer:responseObject];
    } andFailBlock:^(NSURLSessionDataTask *task, NSError *error) {
        [self errorServer:error];
    }];
}

- (IBAction)registerNewUser:(UIButton *)sender {
    NSString *email = self.registerEmailTextField.text;
    NSString *password = self.registerPasswordTextField.text;
    
    APIClient *client = [APIClient sharedClient];
    [client registerUserWithName:email andPassword:password withSuccessBlock:^(NSURLSessionDataTask *task, id responseObject) {
        [self errorServer:responseObject];
        [UIView animateWithDuration:0.3 animations:^{
            self.registrationView.alpha = 0;
            self.restorationPasswordView.alpha = 0;
        }];
    } andFailBlock:nil];
}

- (IBAction)restorationPassword:(UIButton *)sender {
     NSString *email = self.restorationPassword.text;
    APIClient *client = [APIClient sharedClient];
    [client restorationPasswordWithMail:email withSuccessBlock:^(NSURLSessionDataTask *task, id responseObject) {
        [self errorServer:responseObject];
        [UIView animateWithDuration:0.3 animations:^{
            self.registrationView.alpha = 0;
            self.restorationPasswordView.alpha = 0;
        }];
    } andFailBlock:nil];
}

- (IBAction)cancelRestoration:(UIButton *)sender {
    [UIView animateWithDuration:0.3 animations:^{
        self.registrationView.alpha = 0;
        self.restorationPasswordView.alpha = 0;
    }];
}

#pragma mark - UITextFild Delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if ([textField isEqual:self.registrationTextField]) {
        [UIView animateWithDuration:0.3 animations:^{
            self.registrationView.alpha = 1;
            self.restorationPasswordView.alpha = 0;
        }];
        [self.registrationTextField resignFirstResponder];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)errorServer:(id)responseObject {
    if (responseObject[@"error"] != [NSNull null] ) {
        NSDictionary *errorArray = responseObject[@"error"];
        NSInteger errorCode = [errorArray[@"code"] integerValue];
        NSLog(@"%li Description: %@", (long)errorCode, errorArray[@"description"]);
        [self alertWithMessages:errorArray[@"description"] andTitle:@"Ошибка!"];
    } else {
        NSDictionary *userInfo = responseObject[@"user_info"];
        NSLog(@"%@", responseObject);
        LoginModel *user = [LoginModel deserializeAppInfosFromJSON:userInfo];
        user.userPass = self.passwordTextField.text;
        if (user) {
            NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:user];
            [[NSUserDefaults standardUserDefaults] setObject:responseObject[@"auth_token"] forKey:@"token"];
            [[NSUserDefaults standardUserDefaults] setObject:encodedObject forKey:@"userLogin"];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isLogin"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
}

- (void)alertWithMessages:(NSString *)messages andTitle:(NSString *)title {
    [[[UIAlertView alloc] initWithTitle:title
                                message:messages
                               delegate:self
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil, nil] show ];
}


@end
