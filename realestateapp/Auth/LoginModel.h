//
//  LoginModel.h
//  realestateapp
//
//  Created by Юрий Дурнев on 27.07.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoginModel : MTLModel <MTLJSONSerializing>
@property (nonatomic, copy) NSString *userMail;
@property (nonatomic, copy) NSString *userPass;
@property (nonatomic, copy) NSString *userSkype;
@property (nonatomic, copy) NSString *userFirstName;
@property (nonatomic, copy) NSString *userLastName;
@property (nonatomic, copy) NSString *userPhone;


+ (LoginModel *)deserializeAppInfosFromJSON:(NSDictionary *)appInfosJSON;
@end
