//
//  ColorNameCell.h
//  CollectionViewItemAnimations
//
//  Created by Nick Donaldson on 8/27/13.
//  Copyright (c) 2013 nd. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface ColorNameCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imageViewAdvert;

@end
