//
//  RLAlertNotificationView.m
//  realestateapp
//
//  Created by Юрий Дурнев on 24.08.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "RLAlertNotificationView.h"

@implementation RLAlertNotificationView

+ (RLAlertNotificationView *)sharedClient {
    static RLAlertNotificationView *sharedHTTPClient = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedHTTPClient = [[self alloc] init];
    });
    
    return sharedHTTPClient;
}

- (void)addedMessageTitle:(NSString *)title message:(NSString *)message onView:(UIViewController *)viewController {
    [TSMessage showNotificationInViewController:viewController
                                          title:NSLocalizedString(title, nil)
                                       subtitle:NSLocalizedString(message, nil)
                                          image:nil
                                           type:TSMessageNotificationTypeSuccess
                                       duration:TSMessageNotificationDurationAutomatic
                                       callback:nil
                                    buttonTitle:nil
                                 buttonCallback:nil
                                     atPosition:TSMessageNotificationPositionTop
                           canBeDismissedByUser:YES];
}

- (void)addedErrorMessageTitle:(NSString *)title message:(NSString *)message onView:(UIViewController *)viewController {
    [TSMessage showNotificationInViewController:viewController
                                          title:NSLocalizedString(title, nil)
                                       subtitle:NSLocalizedString(message, nil)
                                          image:nil
                                           type:TSMessageNotificationTypeError
                                       duration:TSMessageNotificationDurationAutomatic
                                       callback:nil
                                    buttonTitle:nil
                                 buttonCallback:nil
                                     atPosition:TSMessageNotificationPositionTop
                           canBeDismissedByUser:YES];
}

@end
