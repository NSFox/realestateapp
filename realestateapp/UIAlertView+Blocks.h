//
//  UIAlertView+Blocks.h
//  Shibui
//
//  Created by Jiva DeVoe on 12/28/10.
//  Copyright 2010 Random Ideas, LLC. All rights reserved.
//  Modified by Robert Saunders on 20/01/12
//

#import <Foundation/Foundation.h>

@interface UIAlertView (Blocks)

+ (void)displayAlertWithTitle:(NSString *)title
                       message:(NSString *)message
               leftButtonTitle:(NSString *)leftButtonTitle
              leftButtonAction:(void (^)(void))leftButtonAction
              rightButtonTitle:(NSString *)rightButtonTitle
             rightButtonAction:(void (^)(void))rightButtonAction;

@end
