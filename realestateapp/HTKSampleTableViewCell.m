//
//  HTKSampleTableViewCell.m
//  HTKDynamicResizingCell
//
//  Created by Henry T Kirk on 11/1/14.
//
//  Copyright (c) 2014 Henry T. Kirk (http://www.henrytkirk.info)
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.


#import "HTKSampleTableViewCell.h"

@interface HTKSampleTableViewCell ()

/**
 * ImageView that shows sample picture
 */
@property (nonatomic, strong) UIImageView *sampleImageView;

/**
 * Label that is name of sample person
 */
@property (nonatomic, strong) UILabel *nameLabel;

/**
 * Label that is name of sample company
 */
@property (nonatomic, strong) UILabel *companyLabel;

/**
 * Label that displays sample "bio".
 */
@property (nonatomic, strong) UILabel *bioLabel;

@end

@implementation HTKSampleTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupView];
    }
    return self;
}

- (void)setupView {
    
    self.backgroundColor = [UIColor whiteColor];
    self.accessoryType = UITableViewCellAccessoryNone;
    self.accessoryView = nil;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    // Fix for contentView constraint warning
    [self.contentView setAutoresizingMask:UIViewAutoresizingFlexibleHeight];
    
    // Bio label
    self.bioLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.bioLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.bioLabel.numberOfLines = 0; // Must be set for multi-line label to work
    self.bioLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.bioLabel.font = [UIFont fontWithName:@"AvenirNext-Medium" size:14];
    self.bioLabel.textColor = [UIColor blackColor];
    [self.contentView addSubview:self.bioLabel];
    
    // Constrain
    NSDictionary *viewDict = NSDictionaryOfVariableBindings(_bioLabel);
    // Create a dictionary with buffer values
    NSDictionary *metricDict = @{@"sideBuffer" : @15, @"verticalBuffer" : @0};
    

    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-sideBuffer-[_bioLabel]-sideBuffer-|" options:0 metrics:metricDict views:viewDict]];

    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-verticalBuffer-[_bioLabel]-verticalBuffer-|" options:0 metrics:metricDict views:viewDict]];

    [self.bioLabel setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
    [self.bioLabel setContentCompressionResistancePriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisHorizontal];

    CGSize defaultSize = DEFAULT_CELL_SIZE;
    self.bioLabel.preferredMaxLayoutWidth = defaultSize.width - ([metricDict[@"sideBuffer"] floatValue] * 4);
}

- (void)setupCellWithData:(NSString *)data {
    
    if ([data length] != 0 || data != nil) {
        self.bioLabel.text = data;
    }
}

@end
