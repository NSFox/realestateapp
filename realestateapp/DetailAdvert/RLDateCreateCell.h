//
//  RLDateCreateCell.h
//  realestateapp
//
//  Created by Юрий Дурнев on 29.07.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailAdvertModel.h"

@interface RLDateCreateCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *dateText;
@property (weak, nonatomic) IBOutlet UILabel *viewAdvertCount;
@property (weak, nonatomic) IBOutlet UIButton *buyPurchase;

- (void)setDateFromCell:(DetailAdvertModel *)model;
@end
