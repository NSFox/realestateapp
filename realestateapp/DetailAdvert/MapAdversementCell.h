//
//  MapAdversementCell.h
//  realestateapp
//
//  Created by NSFox on 05.05.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MapAdversementCell : UITableViewCell <MKMapViewDelegate>
@property (weak, nonatomic) IBOutlet MKMapView *advertMap;
@property (weak, nonatomic) IBOutlet YIInnerShadowView *shadowView;
- (void)addAnnotation:(CLLocationCoordinate2D)location;
@end
