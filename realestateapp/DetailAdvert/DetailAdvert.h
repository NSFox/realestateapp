//
//  DetailAdvert.h
//  realestateapp
//
//  Created by NSFox on 05.05.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MAdvert.h"
#import "AdvertListViewController.h"

@class DetailAdvertModel;

@interface DetailAdvert : UIViewController
@property (strong, nonatomic, readonly) DetailAdvertModel *advertModel;
@property (strong, nonatomic) NSString *adversementID;
@property (strong, nonatomic) NSString *mapStreet;
@property (strong, nonatomic) NSString *homeNum;
@property (strong, nonatomic) NSString *metro;
@end
