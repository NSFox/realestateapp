//
//  RLDateCreateCell.m
//  realestateapp
//
//  Created by Юрий Дурнев on 29.07.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "RLDateCreateCell.h"

@implementation RLDateCreateCell

- (void)awakeFromNib {
    // Initialization code
    self.buyPurchase.layer.borderWidth = 0.5;
    self.buyPurchase.layer.borderColor = [UIColor colorWithRed:0.961 green:0.294 blue:0.318 alpha:1.000].CGColor;
    self.buyPurchase.layer.cornerRadius = 5;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setDateFromCell:(DetailAdvertModel *)model {
    NSString *date = model.dataCreate;
    self.dateText.text = [self formatDateAtStringToString:date];
    self.viewAdvertCount.text = [NSString stringWithFormat:@"%@ просмотров", model.viewsCount];
}

- (NSString *)formatDateAtStringToString:(NSString *)formatString {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    
    NSDateFormatter *currentFormatter = [NSDateFormatter new];
    currentFormatter.dateStyle = NSDateIntervalFormatterLongStyle;
    currentFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"ru_RU_POSIX"];
    return [currentFormatter stringFromDate:[formatter dateFromString:formatString]];
}

@end
