//
//  IBView.h
//  realestateapp
//
//  Created by NSFox on 05.05.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IBView : UIView
@property (nonatomic) IBInspectable CGFloat borderWidth;
@property (nonatomic) IBInspectable CGFloat cornerRadius;

@end
