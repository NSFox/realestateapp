//
//  ContactCell.h
//  realestateapp
//
//  Created by NSFox on 06.05.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *emailButton;
@property (weak, nonatomic) IBOutlet UIButton *callButton;
@property (strong, nonatomic) NSString *emailString;
@property (strong, nonatomic) NSString *callString;
@property (weak, nonatomic) IBOutlet UILabel *nameSeller;
@property (weak, nonatomic) IBOutlet UIButton *sendReason;

@end
