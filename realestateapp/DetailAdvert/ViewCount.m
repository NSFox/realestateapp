//
//  ViewCount.m
//  realestateapp
//
//  Created by NSFox on 13.05.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "ViewCount.h"

@implementation ViewCount
+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"viewCount": @"date",
             @"dataString": @"views"
             };
}

+ (NSArray *)deserializeAppInfosFromJSON:(NSArray *)appInfosJSON {
    NSError *error;
    
    NSArray *arrayModel = [MTLJSONAdapter modelsOfClass:[ViewCount class] fromJSONArray:appInfosJSON error:&error];
    if (error) {
        NSLog(@"Couldn't convert app infos JSON to DetailAdvertModel models: %@", error);
        return nil;
    }
    
    return arrayModel;
}

@end
