//
//  ScrollAdvertCell.m
//  realestateapp
//
//  Created by NSFox on 05.05.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "ScrollAdvertCell.h"

@implementation ScrollAdvertCell

- (void)awakeFromNib {
    self.priceView.alpha = 0.8;
    self.countView.alpha = 0.8;
    _scrollView.continuous = NO;
    _scrollView.autoPlayTimeInterval = 5.f;
    UIBezierPath *maskPath;
    maskPath = [UIBezierPath bezierPathWithRoundedRect:_priceLabel.bounds
                                     byRoundingCorners:(UIRectCornerBottomLeft|UIRectCornerBottomRight)
                                           cornerRadii:CGSizeMake(0.0, 5.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.backgroundColor = [UIColor blackColor].CGColor;
    maskLayer.frame = self.bounds;
    maskLayer.path = maskPath.CGPath;
    _priceLabel.layer.mask = maskLayer;
    _priceLabel.layer.masksToBounds = YES;
}

#pragma mark - KDCycleBannerViewDataource

- (NSArray *)numberOfKDCycleBannerView:(KDCycleBannerView *)bannerView {
    
    return self.dataSourceScroll;
}

- (UIViewContentMode)contentModeForImageIndex:(NSUInteger)index {
    return UIViewContentModeScaleAspectFill;
}

- (UIImage *)placeHolderImageOfZeroBannerView {
    return [UIImage imageNamed:@"placeholder"];
}

#pragma mark - KDCycleBannerViewDelegate

- (void)cycleBannerView:(KDCycleBannerView *)bannerView didScrollToIndex:(NSUInteger)index {
    NSLog(@"%d", (int)index + 1);
    _countLabel.text = [NSString stringWithFormat:@"%d/%d", (int)index + 1, (int)self.dataSourceScroll.count];
}

- (void)cycleBannerView:(KDCycleBannerView *)bannerView didSelectedAtIndex:(NSUInteger)index {
}

@end
