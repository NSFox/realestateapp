//
//  ViewCount.h
//  realestateapp
//
//  Created by NSFox on 13.05.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "MTLModel.h"

@interface ViewCount : MTLModel <MTLJSONSerializing>

@property (copy, readonly, nonatomic) NSString *viewCount;
@property (copy, readonly, nonatomic) NSString *dataString;

+ (NSArray *)deserializeAppInfosFromJSON:(NSArray *)appInfosJSON;

@end
