//
//  MapAdversementCell.m
//  realestateapp
//
//  Created by NSFox on 05.05.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "MapAdversementCell.h"
#import "CustomOverlayView.h"
#import "OSMTileOverlay.h"
#import "TSBathroomAnnotation.h"

@implementation MapAdversementCell

- (void)awakeFromNib {
    OSMTileOverlay *overlay = [[OSMTileOverlay alloc] init];
    [self.advertMap addOverlay:overlay];
    self.advertMap.delegate = self;
    _shadowView.shadowRadius = 3;
    _shadowView.shadowOpacity = 0.5;
    _shadowView.shadowMask = YIInnerShadowMaskAll;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

#pragma mark MKMapViewDelegate

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay {
    CustomOverlayView *overlayView = [[CustomOverlayView alloc] initWithOverlay:overlay];
    return overlayView;
}

- (void)addAnnotation:(CLLocationCoordinate2D)location {
    [self.advertMap removeAnnotations:self.advertMap.annotations];
    TSBathroomAnnotation *annotation = [[TSBathroomAnnotation alloc] initWithCoordinates:location
                                                                                   title:nil
                                                                                subtitle:nil];
    [self.advertMap addAnnotation:annotation];

    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(location, 100, 100);
    [self.advertMap setRegion:[self.advertMap regionThatFits:region] animated:NO];
}

@end
