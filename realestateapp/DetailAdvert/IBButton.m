//
//  IBButton.m
//  realestateapp
//
//  Created by NSFox on 06.05.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "IBButton.h"

IB_DESIGNABLE
@implementation IBButton

- (void)setCornerRadius:(CGFloat)cornerRadius {
    _cornerRadius = cornerRadius;
    [self setupView];
}

- (void)setBorderWidth:(CGFloat)borderWidth {
    _borderWidth = borderWidth;
    [self setupView];
}

- (void)setBorderColor:(UIColor *)borderColor {
    _borderColor = borderColor;
    [self setupView];
}

#pragma mark - Overrides
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupDefaultValues];
        [self setupView];
    }
    return self;
}

#pragma mark - Initializer
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setupDefaultValues];
        [self setupView];
    }
    return self;
}


#pragma mark - Internal functions
- (void)setupDefaultValues {
    self.cornerRadius = 0.0;
    self.borderWidth = 0.0;
    self.borderColor = [UIColor blackColor];
}

- (void)setupView {
    self.layer.cornerRadius = self.cornerRadius;
    self.layer.borderWidth = self.borderWidth;
    self.layer.borderColor = self.borderColor.CGColor;
    [self setNeedsDisplay];
}
@end
