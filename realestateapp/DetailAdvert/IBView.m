//
//  IBView.m
//  realestateapp
//
//  Created by NSFox on 05.05.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "IBView.h"

IB_DESIGNABLE
@implementation IBView

#pragma mark - Inspectable properties

- (void)setBorderWidth:(CGFloat)borderWidth {
    _borderWidth = borderWidth;
    [self setupView];
}
- (void)setCornerRadius:(CGFloat)cornerRadius {
    _cornerRadius = cornerRadius;
    [self setupView];
}

#pragma mark - Overrides
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupDefaultValues];
        [self setupView];
    }
    return self;
}

#pragma mark - Initializer
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setupDefaultValues];
        [self setupView];
    }
    return self;
}

#pragma mark - Internal functions
- (void)setupDefaultValues {
    self.cornerRadius = 0.0;
    self.borderWidth = 0.0;
}

- (void)setupView {
    self.layer.borderWidth = self.borderWidth;
    self.layer.cornerRadius = self.cornerRadius;
    [self setNeedsDisplay];
}

@end
