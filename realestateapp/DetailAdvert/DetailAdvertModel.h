//
//  DetailAdvertModel.h
//  realestateapp
//
//  Created by NSFox on 05.05.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "MTLModel.h"


@interface CustomFields : MTLModel <MTLJSONSerializing>
@property (copy, readonly, nonatomic) NSString *name;
@property (copy, readonly, nonatomic) NSString *title;
@property (copy, readonly, nonatomic) NSString *value;

+ (NSArray *)deserializeAppInfosFromJSON:(NSArray *)appInfosJSON;
@end

@interface ImageModel : MTLModel <MTLJSONSerializing>
@property (copy, readonly, nonatomic) NSString *orig;
@property (copy, readonly, nonatomic) NSString *mobile;
@end

@interface GroupCustomFields : MTLModel <MTLJSONSerializing>
@property (copy, readonly, nonatomic) NSString *title;
@property (copy, readonly, nonatomic) NSString *sortIndex;
@property (copy, readonly, nonatomic) NSArray *customFields;

+ (NSArray *)deserializeGroupCustomFieldsJSON:(NSArray *)appInfosJSON;

@end

@interface DetailAdvertModel : MTLModel <MTLJSONSerializing>
@property (copy, readonly, nonatomic) NSString *adversementID;
@property (copy, readonly, nonatomic) NSString *dataCreate;
@property (copy, readonly, nonatomic) NSString *currency;
@property (copy, readonly, nonatomic) NSString *region;
@property (copy, readonly, nonatomic) NSString *metro;
@property (copy, readonly, nonatomic) NSString *mapStreet;
@property (copy, readonly, nonatomic) NSString *mapHouseNr;
@property (copy, readonly, nonatomic) NSString *title;
@property (copy, readonly, nonatomic) NSString *text;
@property (copy, readonly, nonatomic) NSString *seller;
@property (copy, readonly, nonatomic) NSString *category;
@property (copy, readonly, nonatomic) NSString *categoryUrl;
@property (copy, readonly, nonatomic) NSString *user;
@property (copy, readonly, nonatomic) NSString *email;
@property (copy, readonly, nonatomic) NSArray *phoneArr;
@property (copy, readonly, nonatomic) NSArray *images;
@property (copy, readonly, nonatomic) NSArray *groupCustomFields;
@property (copy, readonly, nonatomic) NSString *name;
@property (nonatomic) CLLocationCoordinate2D locationCoordinate;
@property (copy, readonly, nonatomic) NSString *geoLat;
@property (copy, readonly, nonatomic) NSString *geoLng;
@property (assign, readonly, nonatomic) NSInteger price;
@property (copy, readonly, nonatomic) NSString *rooms;
@property (copy, readonly, nonatomic) NSString *viewsCount;
@property (copy, readonly, nonatomic) NSString *isFavorited;
@property (copy, readonly, nonatomic) NSString *isPremium;

+ (DetailAdvertModel *)deserializeAppInfosFromJSON:(NSDictionary *)appInfosJSON;

@end


