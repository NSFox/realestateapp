//
//  IBButton.h
//  realestateapp
//
//  Created by NSFox on 06.05.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IBButton : UIButton
@property (nonatomic) IBInspectable CGFloat cornerRadius;
@property (nonatomic) IBInspectable CGFloat borderWidth;
@property (nonatomic) IBInspectable UIColor *borderColor;
@end
