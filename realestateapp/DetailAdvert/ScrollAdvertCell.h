//
//  ScrollAdvertCell.h
//  realestateapp
//
//  Created by NSFox on 05.05.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <KDCycleBannerView.h>

@interface ScrollAdvertCell : UIView <KDCycleBannerViewDataource, KDCycleBannerViewDelegate>
@property (weak, nonatomic) IBOutlet KDCycleBannerView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *priceView;
@property (weak, nonatomic) IBOutlet UIView *countView;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;

@property (strong, nonatomic) NSArray *dataSourceScroll;

@end
