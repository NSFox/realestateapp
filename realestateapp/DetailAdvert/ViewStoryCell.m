//
//  ViewStoryCell.m
//  realestateapp
//
//  Created by NSFox on 06.05.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "ViewStoryCell.h"
#import "ViewCount.h"

@implementation ViewStoryCell

- (void)awakeFromNib {
    // Initialization code
    NSDictionary *_themeAttributes = @{
                                       kXAxisLabelColorKey : [UIColor blackColor],
                                       kXAxisLabelFontKey : [UIFont fontWithName:@"AvenirNext-Medium" size:10],
                                       kYAxisLabelColorKey : [UIColor blackColor],
                                       kYAxisLabelFontKey : [UIFont fontWithName:@"AvenirNext-Medium" size:10],
                                       kYAxisLabelSideMarginsKey : @10,
                                       kPlotBackgroundLineColorKye : [UIColor colorWithWhite:0.538 alpha:1.000],
                                       };
    _chartView.themeAttributes = _themeAttributes;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)arrayViewCountSotring:(NSArray *)viewConut {
    NSMutableArray *addCount = [NSMutableArray array];
    NSMutableArray *addDate = [NSMutableArray array];
    self.viewCount = [NSMutableArray array];
    self.dataCount = [NSMutableArray array];
    
    if (viewConut.count >= 7) {
        NSArray *arrayReverce = [[viewConut reverseObjectEnumerator] allObjects];
        for (NSUInteger i = 0; i < 7; i++) {
            ViewCount *VC = arrayReverce[i];
            [addCount addObject:VC.viewCount];
            [addDate addObject:VC.dataString];
        }
        self.viewCount.array = [[addCount reverseObjectEnumerator] allObjects];
        self.dataCount.array = [[addDate reverseObjectEnumerator] allObjects];
    } else {
        for (NSUInteger i = 0; i < viewConut.count; i++) {
            ViewCount *VC = viewConut[i];
            [self.viewCount addObject:VC.viewCount];
            [self.dataCount addObject:VC.dataString];
        }
    }
    for (NSUInteger i = 0; i < self.viewCount.count; i++) {
        NSString *formDate = [self formatDateAtStringToString:self.viewCount[i]];
        [self.viewCount replaceObjectAtIndex:i withObject:formDate];
    }
    if (_chartView) {
        [_chartView removeFromSuperview];
    }
    
    _chartView = [[SHLineGraphView alloc] initWithFrame:_chartView.frame];
    [self.contentView addSubview:_chartView];
    
    [self showViewCount];
}

- (void)showViewCount {

    NSMutableArray *xArray = [NSMutableArray new];
    NSMutableArray *pilotArray = [NSMutableArray new];
    NSNumber *max = [self.dataCount valueForKeyPath:@"@max.intValue"];
    
    for (NSUInteger i = 0; i < self.dataCount.count; i++) {
        NSString *viewCount = self.dataCount[i];
        [pilotArray addObject:@{@(i):viewCount}];
    }
    
    for (NSUInteger i = 0; i < self.viewCount.count; i++) {
        NSString *viewCount = self.viewCount[i];
        [xArray addObject:@{@(i):viewCount}];
    }
    
    _chartView.yAxisRange = max;
    _chartView.yAxisSuffix = @"";
    _chartView.xAxisValues = xArray;
    
     SHPlot *_plot1 = [[SHPlot alloc] init];
    
    _plot1.plottingValues = pilotArray;
    _plot1.plottingPointsLabels = self.dataCount;
    
    NSDictionary *_plotThemeAttributes = @{
                                           kPlotFillColorKey : [UIColor colorWithRed:0.412 green:0.706 blue:0.012 alpha:0.500],
                                           kPlotStrokeWidthKey : @2,
                                           kPlotStrokeColorKey : [UIColor colorWithRed:0.412 green:0.706 blue:0.012 alpha:1.000],
                                           kPlotPointFillColorKey : [UIColor colorWithRed:0.961 green:0.294 blue:0.318 alpha:1.000],
                                           kPlotPointValueFontKey : [UIFont fontWithName:@"AvenirNext-Medium" size:18]
                                           };
    
    _plot1.plotThemeAttributes = _plotThemeAttributes;
    [_chartView addPlot:_plot1];
    
    [_chartView setupTheView];
}

- (NSString *)formatDateAtStringToString:(NSString *)formatString {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"dd.MM.yyyy";
    
    NSDateFormatter *currentFormatter = [NSDateFormatter new];
    currentFormatter.dateStyle = NSDateIntervalFormatterLongStyle;
    [currentFormatter setDateFormat:@"dd.MM"];
    currentFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"ru_RU_POSIX"];
    NSString *date = [currentFormatter stringFromDate:[formatter dateFromString:formatString]];
    return date;
}

- (IBAction)purchaseButton:(UIButton *)sender {
}

@end
