//
//  ViewStoryCell.h
//  realestateapp
//
//  Created by NSFox on 06.05.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewStoryCell : UITableViewCell
@property (strong, nonatomic) IBOutlet SHLineGraphView *chartView;
@property (strong, nonatomic) NSMutableArray *viewCount;
@property (strong, nonatomic) NSMutableArray *dataCount;

- (void)arrayViewCountSotring:(NSArray *)viewConut;
@end
