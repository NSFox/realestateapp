//
//  DetailAdvertModel.m
//  realestateapp
//
//  Created by NSFox on 05.05.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "DetailAdvertModel.h"

//////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - GroupCustomFields Model
//////////////////////////////////////////////////////////////////////////////////////////////

@implementation GroupCustomFields

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"title": @"title",
             @"sortIndex": @"sort_index",
             @"customFields" : @"custom_fields"
             };
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue error:(NSError **)error {
    self = [super initWithDictionary:dictionaryValue error:error];
    if (self == nil) return nil;
    _customFields = [CustomFields deserializeAppInfosFromJSON:dictionaryValue[@"customFields"]];
    
    return self;
}

+ (NSArray *)deserializeGroupCustomFieldsJSON:(NSArray *)appInfosJSON {
    NSError *error;

    NSArray *arrayModel = [MTLJSONAdapter modelsOfClass:[GroupCustomFields class] fromJSONArray:appInfosJSON error:&error];
    if (error) {
        NSLog(@"Couldn't convert app infos JSON to DetailAdvertModel models: %@", error);
        return nil;
    }

    return arrayModel;
}

@end

//////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - CustomFields Model
//////////////////////////////////////////////////////////////////////////////////////////////

@implementation CustomFields

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"name": @"name",
             @"title": @"title",
             @"value": @"value"
             };
}

+ (NSArray *)deserializeAppInfosFromJSON:(NSArray *)appInfosJSON {
    NSError *error;

    NSArray *arrayModel = [MTLJSONAdapter modelsOfClass:[CustomFields class] fromJSONArray:appInfosJSON error:&error];
    if (error) {
        NSLog(@"Couldn't convert app infos JSON to DetailAdvertModel models: %@", error);
        return nil;
    }

    return arrayModel;
}

@end

//////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - ImageModel Model
//////////////////////////////////////////////////////////////////////////////////////////////

@implementation ImageModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"orig": @"orig",
             @"mobile": @"mobile"
             };
}

+ (NSArray *)deserializeAppInfosFromJSON:(NSArray *)appInfosJSON {
    NSError *error;
    
    NSArray *arrayModel = [MTLJSONAdapter modelsOfClass:[ImageModel class] fromJSONArray:appInfosJSON error:&error];
    if (error) {
        NSLog(@"Couldn't convert app infos JSON to DetailAdvertModel models: %@", error);
        return nil;
    }
    
    return arrayModel;
}

@end

//////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - DetailAdvertModel Model
//////////////////////////////////////////////////////////////////////////////////////////////

@implementation DetailAdvertModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"adversementID": @"id",
             @"dataCreate": @"date_create",
             @"price": @"price",
             @"currency": @"currency",
             @"region": @"region",
             @"metro": @"metro",
             @"mapStreet": @"mapStreet",
             @"mapHouseNr": @"mapHouseNr",
             @"isPremium": @"ispremium",
             @"title": @"title",
             @"category" : @"category",
             @"categoryUrl" : @"category_url",
             @"groupCustomFields":@"group_custom_fields",
             @"images": @"images",
             @"name": @"name",
             @"text": @"text",
             @"user": @"user",
             @"currency" :@"currency",
             @"seller": @"seller",
             @"geoLat": @"geo_lat",
             @"geoLng": @"geo_lng",
             @"rooms": @"rooms",
             @"viewsCount": @"views_count",
             @"isFavorited": @"isfavorited",
             @"email" : @"email",
             @"phoneArr": @"phone"
             };
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue error:(NSError **)error {
    self = [super initWithDictionary:dictionaryValue error:error];
    if (self == nil) return nil;
    CLLocationDegrees currentLatitude = _geoLat.doubleValue;
    CLLocationDegrees currentLongitude = _geoLng.doubleValue;
    _locationCoordinate = CLLocationCoordinate2DMake(currentLatitude, currentLongitude);
    
    _images = [ImageModel deserializeAppInfosFromJSON:dictionaryValue[@"images"]];
    _groupCustomFields = [GroupCustomFields deserializeGroupCustomFieldsJSON:dictionaryValue[@"groupCustomFields"]];
    
    return self;
}

+ (DetailAdvertModel *)deserializeAppInfosFromJSON:(NSDictionary *)appInfosJSON {
    NSError *error;
    
    DetailAdvertModel *dictModel = [MTLJSONAdapter modelOfClass:[DetailAdvertModel class] fromJSONDictionary:appInfosJSON error:&error];
    if (error) {
        NSLog(@"Couldn't convert app infos JSON to DetailAdvertModel models: %@", error);
        return nil;
    }
    
    return dictModel;
}

@end

