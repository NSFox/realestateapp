//
//  DetailAdvert.m
//  realestateapp
//
//  Created by NSFox on 05.05.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "DetailAdvert.h"
#import "MapAdversementCell.h"
#import "ScrollAdvertCell.h"
#import "TSBathroomAnnotation.h"
#import "DataCell.h"
#import "PlaceCell.h"
#import "ContactCell.h"
#import "DetailAdvertModel.h"
#import "ViewStoryCell.h"
#import "DetailAdvertModel.h"
#import "HTKSampleTableViewCell.h"
#import "ViewCount.h"
#import "RLDateCreateCell.h"
#import "LoginViewController.h"
#import <MessageUI/MessageUI.h>

static NSString *HTKSampleTableViewCellIdentifier = @"HTKSampleTableViewCellIdentifier";

@interface DetailAdvert () <UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate, UIActionSheetDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet ScrollAdvertCell *imageScroll;
@property (strong, nonatomic) DetailAdvertModel *advertModel;
@property (strong, nonatomic) NSArray *viewCount;
@property (assign, nonatomic) BOOL favorite;
@end

@implementation DetailAdvert

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.hidden = YES;
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([MapAdversementCell class]) bundle:nil]
         forCellReuseIdentifier:NSStringFromClass([MapAdversementCell class])];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([RLDateCreateCell class]) bundle:nil]
         forCellReuseIdentifier:NSStringFromClass([RLDateCreateCell class])];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([DataCell class]) bundle:nil]
         forCellReuseIdentifier:NSStringFromClass([DataCell class])];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([PlaceCell class]) bundle:nil]
         forCellReuseIdentifier:NSStringFromClass([PlaceCell class])];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ContactCell class]) bundle:nil]
         forCellReuseIdentifier:NSStringFromClass([ContactCell class])];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ViewStoryCell class]) bundle:nil]
         forCellReuseIdentifier:NSStringFromClass([ViewStoryCell class])];
    [self.tableView registerClass:[HTKSampleTableViewCell class] forCellReuseIdentifier:HTKSampleTableViewCellIdentifier];
    
    self.title = @"Объявление";
    [self loadData];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [TSMessage dismissActiveNotification];
}

- (void)addItemToBar {
    UIBarButtonItem *btnFav;
    if (self.favorite) {
        btnFav = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"starSelected"] style:UIBarButtonItemStylePlain target:self action:@selector(addToFavorite)];
    } else {
        btnFav = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"star"] style:UIBarButtonItemStylePlain target:self action:@selector(addToFavorite)];
    }
   
    UIBarButtonItem *btnShare = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Share"] style:UIBarButtonItemStylePlain target:self action:@selector(share)];
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:btnShare, btnFav, nil]];
}

- (void)addToFavorite {
    BOOL isLogin = [[NSUserDefaults standardUserDefaults] objectForKey:@"isLogin"];
    if (!isLogin) {
        LoginViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"loginViewController"];
        [self presentViewController:vc animated:YES completion:nil];
    } else {
        if (self.favorite) {
            [[APIClient sharedClient] deleteAdertOnFavorite:self.advertModel.adversementID
                                           withSuccessBlock:^(NSURLSessionDataTask *task, id responseObject) {
                                               self.favorite = NO;
                                                [[RLAlertNotificationView sharedClient] addedMessageTitle:@"Внимание" message:@"Объявление удалено из Избранного" onView:self.navigationController];
                                               [self addItemToBar];
                                           } andFailBlock:^(NSURLSessionDataTask *task, NSError *error) {
                                               
                                           }];
        } else {
            [[APIClient sharedClient] postAddedAdvertToFavoriteonID:self.advertModel.adversementID
                                                   withSuccessBlock:^(NSURLSessionDataTask *task, id responseObject) {
                                                       self.favorite = YES;
                                                        [[RLAlertNotificationView sharedClient] addedMessageTitle:@"Внимание" message:@"Объявление добавлено в Избранное" onView:self.navigationController];
                                                       [self addItemToBar];
                                                   } andFailBlock:^(NSURLSessionDataTask *task, NSError *error) {
                                                       
                                                   }];
        }
    }
}

- (void)share {
    
}


- (void)loadData {
    [[APIClient sharedClient] getAdversementOnID:_adversementID
                                withSuccessBlock:^(NSURLSessionDataTask *task, id responseObject) {
                                    self.advertModel = [DetailAdvertModel deserializeAppInfosFromJSON:responseObject[@"advertisement"]];
                                    [self addImageForScroll];
                                    [_imageScroll.scrollView reloadDataWithCompleteBlock:nil];
                                    self.favorite = self.advertModel.isFavorited.boolValue;
                                    [self loadViewCount];
                                    [self addItemToBar];
                                    [self.tableView reloadData];
                                    self.tableView.hidden = NO;
                                } andFailBlock:^(NSURLSessionDataTask *task, NSError *error) {
                                    NSLog(@"%@", error);
                                }];
}

- (void)loadViewCount {
    [[APIClient sharedClient] getViewCountOnID:_adversementID
                              withSuccessBlock:^(NSURLSessionDataTask *task, id responseObject) {
                                  self.viewCount = [ViewCount deserializeAppInfosFromJSON:responseObject[@"views_history"]];
                                 
                              } andFailBlock:^(NSURLSessionDataTask *task, NSError *error) {
                                   NSLog(@"%@", error);
                              }];
}


- (void)addImageForScroll {
    NSMutableArray *imageArray = [NSMutableArray array];
    if (_advertModel.images.count > 0) {
        for (ImageModel *imageURL in _advertModel.images) {
            [imageArray addObject:imageURL.mobile];
        }
    }
    _imageScroll.dataSourceScroll = imageArray;
    _imageScroll.priceLabel.text = [self priceFormatter:@(_advertModel.price)];
    
    _imageScroll.countLabel.text = [NSString stringWithFormat:@"1/%lu", (unsigned long)imageArray.count];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableView DataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 5) {
        MapAdversementCell *mapCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([MapAdversementCell class])
                                                                      forIndexPath:indexPath];
        [mapCell addAnnotation:_advertModel.locationCoordinate];
        return mapCell;
    } else if (indexPath.section == 0) {
        
        RLDateCreateCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([RLDateCreateCell class])
                                                                      forIndexPath:indexPath];
        [cell setDateFromCell:_advertModel];
        return cell;
    } else if (indexPath.section == 1) {
        PlaceCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PlaceCell class])
                                                         forIndexPath:indexPath];
        cell.titleLabel.text = self.mapStreet == nil ? @"Не указано":
            [NSString stringWithFormat:@"%@ %@", self.mapStreet, self.homeNum == nil ? @"" : self.homeNum];
      
        cell.subtitleLabel.text = self.metro == nil ? @"Не указано": self.metro;
        return cell;
        
    } else if (indexPath.section == 2) {
        HTKSampleTableViewCell *cell = (HTKSampleTableViewCell *)[tableView dequeueReusableCellWithIdentifier:HTKSampleTableViewCellIdentifier forIndexPath:indexPath];
        [cell setupCellWithData:_advertModel.text];

        return cell;
    } else if (indexPath.section == 6) {
        ContactCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ContactCell class]) forIndexPath:indexPath];
        cell.emailString = _advertModel.email;
        if (cell.emailString.length != 0) {
            [cell.emailButton addTarget:self action:@selector(sendMail) forControlEvents:UIControlEventTouchUpInside];
        }
        
        if (_advertModel.phoneArr.count > 0) {
            cell.callString = _advertModel.phoneArr.firstObject;
            [cell.callButton addTarget:self action:@selector(callToNumber) forControlEvents:UIControlEventTouchUpInside];
        } else {
            cell.callButton.enabled = NO;
        }
       
        if (_advertModel.seller.length > 0) {
              cell.nameSeller.text = _advertModel.seller;
        } else {
              cell.nameSeller.text = @"Не указано";
        }
        
        cell.callString.length == 0 ? (cell.callButton.enabled = NO) : (cell.callButton.enabled = YES);
        cell.emailString.length == 0 ? (cell.emailButton.enabled = NO) : (cell.emailButton.enabled = YES);
        
        [cell.sendReason addTarget:self action:@selector(sendAlertAdvert) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    } else if (indexPath.section == 7) {
        ViewStoryCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ViewStoryCell class]) forIndexPath:indexPath];
        
        if (self.viewCount.count != 0) {
            [cell arrayViewCountSotring:self.viewCount];
        }
        return cell;
    } else if (indexPath.section == 3) {
        DataCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([DataCell class])
                                                         forIndexPath:indexPath];
        GroupCustomFields *groupFields = _advertModel.groupCustomFields[0];
        CustomFields *fields = groupFields.customFields[indexPath.row];
        cell.textLabel.text = fields.title;
       
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", fields.value];
        cell.textLabel.textColor = [UIColor colorWithRed:0.329 green:0.359 blue:0.392 alpha:1.000];
        cell.detailTextLabel.textColor = [UIColor blackColor];
        return cell;
    } else if (indexPath.section == 4) {
        DataCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([DataCell class])
                                                         forIndexPath:indexPath];
        GroupCustomFields *groupFields = _advertModel.groupCustomFields[2];
        CustomFields *fields = groupFields.customFields[indexPath.row];
        cell.textLabel.text = fields.title;
        if ([fields.value isKindOfClass:[@(YES) class]]) {
            cell.detailTextLabel.text = @"Да";
        } else if ([fields.value isKindOfClass:[@(NO) class]]) {
            cell.detailTextLabel.text = @"Нет";
        } else {
            cell.detailTextLabel.text = fields.value;
        }
        cell.textLabel.textColor = [UIColor colorWithRed:0.329 green:0.359 blue:0.392 alpha:1.000];
        cell.detailTextLabel.textColor = [UIColor blackColor];
        return cell;
    }
    else {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        return cell;
    }
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 3) {
        GroupCustomFields *groupFields = _advertModel.groupCustomFields[0];
        return groupFields.customFields.count;
    }
    if (section == 4) {
        if (_advertModel.groupCustomFields.count >= 2) {
            GroupCustomFields *groupFields = _advertModel.groupCustomFields[2];
            return groupFields.customFields.count;
        }
    }
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 8;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *viewSeparator = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 1)];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(14, 6, tableView.frame.size.width, 20)];
    [label setFont:[UIFont fontWithName:@"AvenirNext-Medium" size:14]];
    [label setTextColor:[UIColor colorWithRed:0.329 green:0.359 blue:0.392 alpha:1.000]];
    if (section != 1) {
        [view addSubview:label];
    }
    [view setBackgroundColor:[UIColor whiteColor]];
    [viewSeparator setBackgroundColor:[UIColor colorWithRed:0.922 green:0.925 blue:0.937 alpha:1.000]];
    
    switch (section) {
        case 1:
            label.text = @"Описание";
            [view addSubview:viewSeparator];
            break;
        case 2:
            label.text = @"Описание";
            [view addSubview:viewSeparator];
            break;
        case 3:
            label.text = @"Основные характеристики";
            [view addSubview:viewSeparator];
            break;
        case 4:
            label.text = @"Подробнее о квартире";
            break;
        case 6:
            [view setBackgroundColor:[UIColor colorWithRed:0.945 green:0.949 blue:0.957 alpha:1.000]];
            label.text = @"Контактные данные";
            break;
        case 7:
            label.text = @"Просмотры за две недели";
            [view addSubview:viewSeparator];
            break;
        default:
            break;
    }
    label.text = [label.text uppercaseString];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
       
        case 0:
            return 60;
            break;
        case 1:
            return 52;
            break;
        case 2:{
            CGSize defaultSize = DEFAULT_CELL_SIZE;
            CGSize cellSize = [HTKSampleTableViewCell sizeForCellWithDefaultSize:defaultSize setupCellBlock:^id(id<HTKDynamicResizingCellProtocol> cellToSetup) {
                [((HTKSampleTableViewCell *)cellToSetup) setupCellWithData:_advertModel.text];
                return cellToSetup;
            }];
            return cellSize.height;
        }
            break;
        case 3:
            return 30;
            break;
        case 4:
            return 30;
            break;
        case 5:
            return 160;
            break;
        case 6:
            return 181;
            break;
        case 7:
            return 200;
            break;
        default:
            break;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 1:
            return 1;
            break;
        case 2:
            return 30;
            break;
        case 3:
            return 30;
            break;
        case 4:
            return 30;
            break;
        case 6:
            return 30;
            break;
        case 7:
            return 30;
            break;
        default:
            break;
    }
    return 0;
}

- (void)callToNumber {
    NSString *title = [NSString stringWithFormat:@"Позвонить по номеру %@", _advertModel.phoneArr.firstObject];
    [UIAlertView displayAlertWithTitle:title
                               message:nil
                       leftButtonTitle:@"Позвонить"
                      leftButtonAction:^{
                          NSString *phone = _advertModel.phoneArr.firstObject;
                          NSArray  *words = [phone componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                          NSString *nospacestring = [words componentsJoinedByString:@""];
                          
                          NSString *phoneNumber = [@"tel://" stringByAppendingString:nospacestring];
                          [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
                          
                      } rightButtonTitle:@"Отмена" rightButtonAction:nil];
}

- (NSString *)priceFormatter:(NSNumber *)price {
    NSNumberFormatter *frmtr = [[NSNumberFormatter alloc] init];
    [frmtr setGroupingSize:3];
    [frmtr setGroupingSeparator:@" "];
    [frmtr setUsesGroupingSeparator:YES];
    NSString *commaString = [frmtr stringFromNumber:price];
    if ([commaString isEqualToString:@"0"]) {
        commaString = @"";
        return commaString;
    } else {
        NSString *currency = @"₽";
        NSString *pruceWithCurrency = [NSString stringWithFormat:@"%@ %@", commaString, currency];
        return pruceWithCurrency;
    }
}

- (void)sendMail {
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *picker = [MFMailComposeViewController new];
        picker.mailComposeDelegate = self;
        
        //Тема письма
        if (_advertModel.title.length != 0) {
            [picker setSubject:self.advertModel.title];
        }
        
        //Получатели
        if (_advertModel.email.length != 0) {
           [picker setToRecipients:@[_advertModel.email]];
        }
        
        [picker setMessageBody:nil isHTML:NO];
        
        [self presentViewController:picker animated:YES completion:nil];
    } else {
        NSString *ccRecipients = @"";
        NSString *subject = @"";
        NSString *recipients = [NSString stringWithFormat:
                                @"mailto:first@example.com?cc=%@&subject=%@",
                                ccRecipients, subject];
        NSString *body = @"&body=";
        
        NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
        email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result {
    switch (result) {
        case MessageComposeResultCancelled:
            break;
        case MessageComposeResultFailed: {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Невозможно отправить SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)sendAlertAdvert {
    AHKActionSheet *actionSheet = [[AHKActionSheet alloc] init];
    [actionSheet addButtonWithTitle:@"Спам, дубль, общая реклама" type:AHKActionSheetButtonTypeDefault handler:^(AHKActionSheet *as) {
        [self postAlert:@"Спам, дубль или общая реклама"];
    }];
    [actionSheet addButtonWithTitle:@"Неверный регион, рубрика" type:AHKActionSheetButtonTypeDefault handler:^(AHKActionSheet *as) {
        [self postAlert:@"Неверный регион, рубрика"];
    }];
    [actionSheet addButtonWithTitle:@"Некорректная цена" type:AHKActionSheetButtonTypeDefault handler:^(AHKActionSheet *as) {
        [self postAlert:@"Некорректная информация, цена, фото"];
    }];
    [actionSheet addButtonWithTitle:@"Мошеничество" type:AHKActionSheetButtonTypeDefault handler:^(AHKActionSheet *as) {
        [self postAlert:@"Мошенничество или противоречит закону"];
    }];
    [actionSheet addButtonWithTitle:@"Компания, а не частное лицо" type:AHKActionSheetButtonTypeDefault handler:^(AHKActionSheet *as) {
        [self postAlert:@"Компания, выдающая себя за частное лицо"];
    }];
    [actionSheet addButtonWithTitle:@"Материал для взрослых" type:AHKActionSheetButtonTypeDefault handler:^(AHKActionSheet *as) {
        [self postAlert:@"Другое"];
    }];
    actionSheet.buttonTextCenteringEnabled = @YES;
    [actionSheet show];
}

- (void)postAlert:(NSString *)title {
    [[APIClient sharedClient] sendReasonOnAdvert:_advertModel.adversementID reason:title withSuccessBlock:^(NSURLSessionDataTask *task, id responseObject) {
        [[RLAlertNotificationView sharedClient] addedMessageTitle:@"Внимание" message:@"Ваша жалоба успешно отправленна и будет рассмотрена нашими модераторами в ближайшее время." onView:self.navigationController];
    } andFailBlock:^(NSURLSessionDataTask *task, NSError *error) {
          [[RLAlertNotificationView sharedClient] addedErrorMessageTitle:@"Внимание" message:@"Ошибка, попробуйте позже" onView:self.navigationController];
    }];
}

@end
