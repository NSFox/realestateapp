//
//  main.m
//  realestateapp
//
//  Created by Юрий Дурнев on 16/02/15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
