//
//  MapView.m
//  realestateapp
//
//  Created by Юрий Дурнев on 31/03/15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "MapView.h"
#import "APIClient.h"
#import "MAdvert.h"
#import "CustomOverlayView.h"
#import "OSMTileOverlay.h"
#import "TSBathroomAnnotation.h"
#import "TSStreetLightAnnotation.h"
#import "TSDemoClusteredAnnotationView.h"

#import "AdvertNote.h"
#import "AdvertNoteCallout.h"
#import "AdvertCalloutViewController.h"
#import "MapCallout.h"
#import "GenericPinAnnotationView.h"
#import "CalloutAnnotationView.h"
#import "CalloutAnnotation.h"

@interface MapView () <TSClusterMapViewDelegate, CLLocationManagerDelegate, MKMapViewDelegate>
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (assign, nonatomic) BOOL oneLoadAnnotationInRegion;
@property (assign, nonatomic) BOOL didFindMyLocation;
@end

@implementation MapView

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if(self = [super initWithCoder:aDecoder]) {
        [self configureMapView];
    }
    
    return self;
}

- (void)awakeFromNib {
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self configureMapView];
    }
    
    return self;
}

#pragma mark - Map Configure
- (void)configureMapView {
    self.didFindMyLocation = false;
    _oneLoadAnnotationInRegion = NO;
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
#ifdef __IPHONE_8_0
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [self.locationManager requestWhenInUseAuthorization];
        }
    }
#endif
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillTerminate:) name:UIApplicationWillTerminateNotification object:nil];
    
    [self.locationManager startUpdatingLocation];
    self.clusterDiscrimination = 1.0;
    OSMTileOverlay *overlay = [[OSMTileOverlay alloc] init];
    [self addOverlay:overlay];
  
    self.showsUserLocation = YES;
    
    UIPanGestureRecognizer *panRec = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(didDragMap:)];
    [panRec setDelegate:self];
    [self addGestureRecognizer:panRec];
    
    self.category = @"real-estate/apartments-sale";
    [self loadAndAddAnnotationOnMap:[self setParamsForMap:self.visibleMapRect]];
}

- (void)appWillEnterForeground:(NSNotification *)note {
    [self.locationManager startUpdatingLocation];
}

- (void)appWillResignActive:(NSNotification *)note {
    [self.locationManager stopUpdatingLocation];
}

- (void)appWillTerminate:(NSNotification*)note {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillTerminateNotification object:nil];
}

- (void)dealloc {
    [self.locationManager stopUpdatingLocation];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (void)didDragMap:(UIGestureRecognizer*)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        if (!self.isDrawingPolygon) {
             [self loadAndAddAnnotationOnMap:[self setParamsForMap:self.visibleMapRect]];
        }
        for (id<MKAnnotation> ann in self.selectedAnnotations) {
            [self deselectAnnotation:ann animated:YES];
        }
        _oneLoadAnnotationInRegion = YES;
    }
}

- (void)loadAndAddAnnotationOnMap:(NSDictionary *)params {
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
    
        [[APIClient sharedClient] getAdvertOnMapCoordinats:params
                        withSuccessBlock:^(NSURLSessionDataTask *task, id responseObject) {
                            
                            [self addAnnoTationOnMap:responseObject];
                            
                        } andFailBlock:^(NSURLSessionDataTask *task, NSError *error) {
                            
                        }];	
//    });
}

- (NSDictionary *)setParamsForMap:(MKMapRect)mRect {
    return @{@"leftbottom_lat": @([self getSWCoordinate:mRect].latitude),
             @"leftbottom_lng": @([self getSWCoordinate:mRect].longitude),
             @"righttop_lat":  @([self getNECoordinate:mRect].latitude),
             @"righttop_lng":  @([self getNECoordinate:mRect].longitude),
             @"group_adverts": @(false),
             @"limit": @(100),
             @"category": self.category};
}

- (void)addAnnoTationOnMap:(id)responseObject {
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
    
        NSArray *result = [MAdvert deserializeAppInfosFromJSON:responseObject[@"advertisements"]];
        
        if (result.count == 0) {

            NSLog(@"Ничего не найдено");
        }
        
        //Add visible items on map in array for Advert List
//        [_advertList removeAllObjects];
        _advertList = [NSMutableArray arrayWithArray:result];
        
        if (!_arrayAnnotations) {
            _arrayAnnotations = [[NSMutableArray alloc] init];
        }
        
        //Remove all objects and annotations if items in array great 3000
        if (_arrayAnnotations.count > 3000) {
            [self removeAnnotations:self.annotations];
            [_arrayAnnotations removeAllObjects];
            _arrayAnnotations = nil;
        }
        
        NSMutableArray *clusterArray = [NSMutableArray array];
        
        //If not add items if count equal zero, logic not working
        if (_arrayAnnotations.count == 0) {
            [clusterArray addObjectsFromArray:result];
        } else {
//            dispatch_async(dispatch_get_main_queue(), ^(void) {
                //Find and add new objects
                for (MAdvert *advertInResult in result){
                    
                    @autoreleasepool {
                        if (advertInResult != nil) {
                            if (![_arrayAnnotations containsObject:advertInResult])
                                [clusterArray addObject:advertInResult];

                        }
                    }
                }
//            });
        }
        
        result = nil;
        
        //Add items in global array
        [_arrayAnnotations addObjectsFromArray:clusterArray];
        
        
        //Create and add new annotations on map
        for (MAdvert *advert in clusterArray) {
            
            @autoreleasepool {
             
                AdvertNote *advertNote = [[AdvertNote alloc] initWithAdvert:advert];
                
                AdvertNoteCallout *advertNoteCallout = [[AdvertNoteCallout alloc] initWithPhoto:advertNote];
                MapCallout * advertCallout = [[MapCallout alloc] initWithCoordinate:advert.locationCoordinate data:advertNoteCallout];

                dispatch_async(dispatch_get_main_queue(), ^{
                    [self addClusteredAnnotation:advertCallout];
                });
                advertCallout = nil;
            }
        }
        
        clusterArray = nil;
//    });
}

- (void)removeAllAnnotationAndReloadAdvert:(NSDictionary *)params {
    [self removeAnnotations:self.annotations];
    [self loadAndAddAnnotationOnMap:params];
}

- (CLLocationCoordinate2D)getNECoordinate:(MKMapRect)mRect{
    return [self getCoordinateFromMapRectanglePoint:MKMapRectGetMaxX(mRect) y:mRect.origin.y];
}

- (CLLocationCoordinate2D)getNWCoordinate:(MKMapRect)mRect{
    return [self getCoordinateFromMapRectanglePoint:MKMapRectGetMinX(mRect) y:mRect.origin.y];
}

- (CLLocationCoordinate2D)getSECoordinate:(MKMapRect)mRect{
    return [self getCoordinateFromMapRectanglePoint:MKMapRectGetMaxX(mRect) y:MKMapRectGetMaxY(mRect)];
}

- (CLLocationCoordinate2D)getSWCoordinate:(MKMapRect)mRect{
    return [self getCoordinateFromMapRectanglePoint:mRect.origin.x y:MKMapRectGetMaxY(mRect)];
}

- (CLLocationCoordinate2D)getCoordinateFromMapRectanglePoint:(double)x y:(double)y{
    MKMapPoint swMapPoint = MKMapPointMake(x, y);
    return MKCoordinateForMapPoint(swMapPoint);
}

- (NSArray *)getBoundingBox:(MKMapRect)mRect{
    CLLocationCoordinate2D bottomLeft = [self getSWCoordinate:mRect];
    CLLocationCoordinate2D topRight = [self getNECoordinate:mRect];
    return @[[NSNumber numberWithDouble:bottomLeft.latitude ],
             [NSNumber numberWithDouble:bottomLeft.longitude],
             [NSNumber numberWithDouble:topRight.latitude],
             [NSNumber numberWithDouble:topRight.longitude]];
}

#pragma mark -
#pragma mark MKMapViewDelegate

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay {
    CustomOverlayView *overlayView = [[CustomOverlayView alloc] initWithOverlay:overlay];
    return overlayView;
}

- (void)showUserLocation {
    NSDictionary *coords = [[NSUserDefaults standardUserDefaults] objectForKey:@"userLocation"];
   
    CLLocation *userLoc = [[CLLocation alloc] initWithLatitude:[coords[@"lat"] doubleValue] longitude:[coords[@"long"] doubleValue]];
    if (userLoc != nil && userLoc.coordinate.latitude != 0.0) {
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLoc.coordinate, 1000, 1000);
        [self setRegion:[self regionThatFits:region] animated:NO];
    } else  {
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(self.userLocation.coordinate, 1000, 1000);
        [self setRegion:[self regionThatFits:region] animated:NO];
    }
}

#pragma mark - Location Manager delegates

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    if (!self.didFindMyLocation) {
        if ([self isUserLocationVisible]) {
            [self loadAndAddAnnotationOnMap:[self setParamsForMap:self.visibleMapRect]];
            [self.locationManager stopUpdatingLocation];
            self.didFindMyLocation = YES;
            
            NSNumber *lat = [NSNumber numberWithDouble:self.userLocation.coordinate.latitude];
            NSNumber *lon = [NSNumber numberWithDouble:self.userLocation.coordinate.longitude];
            NSDictionary *userLocation=@{@"lat":lat,@"long":lon};
            
            [[NSUserDefaults standardUserDefaults] setObject:userLocation forKey:@"userLocation"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        } else {
            [self showUserLocation];
        }
    }
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [self.locationManager startUpdatingLocation];
    } else if (status == kCLAuthorizationStatusDenied) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Location services not authorized"
                                                        message:@"This app needs you to authorize locations services to work."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    } else
        NSLog(@"Wrong location status");
}

@end
