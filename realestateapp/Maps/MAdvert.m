//
//  MAdvert.m
//  realestateapp
//
//  Created by Юрий Дурнев on 31/03/15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "MAdvert.h"

@implementation MAdvert

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"adversementID": @"id",
             @"dataCreate": @"date_create",
             @"price": @"price",
             @"currency": @"currency",
             @"region": @"region",
             @"metro": @"metro",
             @"mapStreet": @"mapStreet",
             @"mapHouseNr": @"mapHouseNr",
             @"isPremium": @"ispremium",
             @"title": @"title",
             @"images": @"images",
             @"name": @"name",
             @"user": @"user",
             @"geoLat": @"geo_lat",
             @"geoLng": @"geo_lng",
             @"rooms": @"rooms",
             @"viewsCount": @"views_count",
             @"isFavorited": @"isfavorited",
             @"status" : @"status"
             };
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue error:(NSError **)error {
    self = [super initWithDictionary:dictionaryValue error:error];
    if (self == nil) return nil;
    CLLocationDegrees currentLatitude = _geoLat.doubleValue;
    CLLocationDegrees currentLongitude = _geoLng.doubleValue;
    _locationCoordinate = CLLocationCoordinate2DMake(currentLatitude, currentLongitude);
    
    return self;
}

+ (NSArray *)deserializeAppInfosFromJSON:(NSArray *)appInfosJSON {
    NSError *error;
    
    NSArray *arrayModel = [MTLJSONAdapter modelsOfClass:[MAdvert class] fromJSONArray:appInfosJSON error:&error];
    if (error) {
        NSLog(@"Couldn't convert app infos JSON to MAdvert models: %@", error);
        return nil;
    }
    
    return arrayModel;
}

@end
