//
//  AdvertCalloutViewController.h
//  realestateapp
//
//  Created by Юрий Дурнев on 01.09.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AdvertNote.h"

@protocol AdvertCalloutViewControllerDelegate <NSObject>
- (void)toDetailAdvertID:(NSString *)advertID mapStreet:(NSString *)mapStreet homeNum:(NSString *)homeNum metro:(NSString *)metro;
@end

@interface AdvertCalloutViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *placeLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewPin;
@property (weak, nonatomic) IBOutlet UILabel *roomsLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIButton *buttonCall;
@property (weak, nonatomic) IBOutlet UILabel *metroLabel;
@property (weak, nonatomic) IBOutlet UILabel *areaLabel;
@property (weak, nonatomic) IBOutlet UILabel *levelLabel;
@property (strong, nonatomic) AdvertNote * advert;

@property (nonatomic, weak) id <AdvertCalloutViewControllerDelegate> delegate;
@end
