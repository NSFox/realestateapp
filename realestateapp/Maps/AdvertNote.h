//
//  AdvertNote.h
//  realestateapp
//
//  Created by Юрий Дурнев on 01.09.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MAdvert.h"

@interface AdvertNote : NSObject
@property (strong, nonatomic)  MAdvert *advert;
- (instancetype)initWithAdvert:(MAdvert *)advert;
@end
