//
//  AdvertNoteCallout.h
//  realestateapp
//
//  Created by Юрий Дурнев on 01.09.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AdvertNote.h"

@interface AdvertNoteCallout : NSObject
@property (nonatomic, copy) AdvertNote *advertNote;
- (UIViewController *) calloutCell;
- (instancetype)initWithPhoto:(AdvertNote *) photo;

@end
