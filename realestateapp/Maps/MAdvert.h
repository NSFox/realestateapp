//
//  MAdvert.h
//  realestateapp
//
//  Created by Юрий Дурнев on 31/03/15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MAdvert : MTLModel <MTLJSONSerializing>

@property (copy, readonly, nonatomic) NSString *adversementID;
@property (copy, readonly, nonatomic) NSString *dataCreate;
@property (copy, readonly, nonatomic) NSString *currency;
@property (copy, readonly, nonatomic) NSString *region;
@property (copy, readonly, nonatomic) NSString *metro;
@property (copy, readonly, nonatomic) NSString *mapStreet;
@property (copy, readonly, nonatomic) NSString *mapHouseNr;
@property (copy, readonly, nonatomic) NSString *title;
@property (copy, readonly, nonatomic) NSArray *images;
@property (copy, readonly, nonatomic) NSString *name;
@property (copy, readonly, nonatomic) NSString *user;
@property (nonatomic) CLLocationCoordinate2D locationCoordinate;
@property (copy, readonly, nonatomic) NSString *geoLat;
@property (copy, readonly, nonatomic) NSString *geoLng;
@property (assign, readonly, nonatomic) NSInteger price;
@property (copy, readonly, nonatomic) NSString *status;
@property (copy, readonly, nonatomic) NSNumber *rooms;
@property (copy, readonly, nonatomic) NSString *viewsCount;
@property (copy, readonly, nonatomic) NSString *isFavorited;
@property (copy, readonly, nonatomic) NSString *isPremium;

+ (NSArray *)deserializeAppInfosFromJSON:(NSArray *)appInfosJSON;

@end
