//
//  MapsViewController.h
//  realestateapp
//
//  Created by Юрий Дурнев on 23/03/15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MapsViewController : UIViewController
- (void)touchesBegan:(UITouch*)touch;
- (void)touchesMoved:(UITouch*)touch;
- (void)touchesEnded:(UITouch*)touch;
@end
