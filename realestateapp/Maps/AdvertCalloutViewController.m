//
//  AdvertCalloutViewController.m
//  realestateapp
//
//  Created by Юрий Дурнев on 01.09.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "AdvertCalloutViewController.h"
#import "DetailAdvert.h"


@implementation AdvertCalloutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *place = _advert.advert.mapStreet == nil && _advert.advert.mapHouseNr == nil ? @"Не указано":
    [NSString stringWithFormat:@"%@ %@", _advert.advert.mapStreet, _advert.advert.mapHouseNr == nil ? @"" : _advert.advert.mapHouseNr];
    NSString *imageURL;
    if (_advert.advert.images.count > 0) {
        NSDictionary *image = _advert.advert.images[0];
        
        if (image != nil) {
            imageURL = image[@"mobile"];
        }
    }
    
    self.placeLabel.text = place;
    self.titleLabel.text = _advert.advert.title;
    [self.imageViewPin sd_setImageWithURL:[NSURL URLWithString:imageURL.length != 0 ? imageURL : nil]];
    self.roomsLabel.text = _advert.advert.rooms == nil ? @"-" : [NSString stringWithFormat:@"%@ ком.", _advert.advert.rooms];
    self.priceLabel.text = [self priceFormatter:[NSNumber numberWithInteger:_advert.advert.price]];
    self.areaLabel.text = [NSString stringWithFormat:@"100 м\u00B2"];
}

- (IBAction)tapToAdvert:(id)sender {
    SEL selector = @selector(toDetailAdvertID:mapStreet:homeNum:metro:);
    if (self.delegate && [self.delegate respondsToSelector:selector]) {
        [self.delegate toDetailAdvertID:_advert.advert.adversementID
                              mapStreet:_advert.advert.mapStreet
                                homeNum:_advert.advert.mapHouseNr
                                  metro:_advert.advert.metro];
    }
}

- (NSString *)priceFormatter:(NSNumber *)price {
    NSNumberFormatter *frmtr;
    if (frmtr == nil) {
          frmtr = [[NSNumberFormatter alloc] init];
    }
 
    [frmtr setGroupingSize:3];
    [frmtr setGroupingSeparator:@" "];
    [frmtr setUsesGroupingSeparator:YES];
    NSString *commaString = [frmtr stringFromNumber:price];
    if ([commaString isEqualToString:@"0"]) {
        commaString = @"";
        return commaString;
    } else {
        NSString *currency = @"р";
        NSString *pruceWithCurrency = [NSString stringWithFormat:@"%@ %@", commaString, currency];
        return pruceWithCurrency;
    }
}

@end
