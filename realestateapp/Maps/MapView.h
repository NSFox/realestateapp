//
//  MapView.h
//  realestateapp
//
//  Created by Юрий Дурнев on 31/03/15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "YMKMapView.h"

@interface MapView : TSClusterMapView
@property (strong, nonatomic) NSMutableArray *advertList;
@property (strong, nonatomic) NSString *category;
@property (nonatomic) BOOL isDrawingPolygon;
@property (strong, nonatomic) NSMutableArray *arrayAnnotations;
- (void)showUserLocation;
- (void)removeAllAnnotationAndReloadAdvert:(NSDictionary *)params;
- (NSDictionary *)setParamsForMap:(MKMapRect)mRect;
@end
