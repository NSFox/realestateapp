//
//  AdvertNote.m
//  realestateapp
//
//  Created by Юрий Дурнев on 01.09.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "AdvertNote.h"

@implementation AdvertNote

- (instancetype)initWithAdvert:(MAdvert *)advert
{
    self = [super init];
    if (self) {
        _advert = advert;
    }
    return self;
}
@end
