//
//  MapsViewController.m
//  realestateapp
//
//  Created by Юрий Дурнев on 23/03/15.
//  Copyright (c) 2015 irr. All rights reserved.
//

@import MapKit;
#import "MapsViewController.h"
#import "SINavigationMenuView.h"
#import "APIClient.h"
#import "MAdvert.h"
#import "MapView.h"
#import "AdvertListViewController.h"
#import "UIViewController+MaterialDesign.h"
#import "TSBathroomAnnotation.h"
#import "TSDemoClusteredAnnotationView.h"
#import "UIButton+Block.h"

#import "AdvertNote.h"
#import "AdvertNoteCallout.h"
#import "AdvertCalloutViewController.h"
#import "MapCallout.h"
#import "GenericPinAnnotationView.h"
#import "CalloutAnnotationView.h"
#import "CalloutAnnotation.h"
#import "DetailAdvert.h"
#import "CanvasView.h"

#import "FiltersViewController.h"

@interface MapsViewController () <UITextFieldDelegate, AdvertCalloutViewControllerDelegate>
@property (weak, nonatomic) IBOutlet MapView *mapView;
@property (weak, nonatomic) IBOutlet UIButton *locationButton;
@property (weak, nonatomic) IBOutlet UIButton *polygoneButton;
@property (weak, nonatomic) IBOutlet UIButton *zoomIn;
@property (weak, nonatomic) IBOutlet UIButton *zoomOut;
@property (weak, nonatomic) IBOutlet UIButton *listAdvert;
@property (weak, nonatomic) IBOutlet UIView *viewSearch;
@property (weak, nonatomic) IBOutlet UITextField *searchGeoTextField;
@property (strong, nonatomic) LGFilterView  *filterView1;
@property (strong, nonatomic) LGButton  *titleButton;
@property (strong, nonatomic) NSArray   *titlesArray;
@property (strong, nonatomic) NSArray   *categoryArray;
@property (strong, nonatomic) UIView *innerView;
@property (nonatomic,strong) MKAnnotationView *selectedAnnotationView;
@property (nonatomic,strong) CalloutAnnotation *calloutAnnotation;

@property (nonatomic, strong) NSMutableArray *coordinates;
@property (nonatomic, strong) NSMutableArray *polygonPoints;
@property (nonatomic) BOOL isDrawingPolygon;
@property (nonatomic, strong) CanvasView *canvasView;

@end

@implementation MapsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupMenu];
    [self setupInterface];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if (self.isViewLoaded && _mapView && _mapView.annotations)
    {
        [_mapView removeAnnotations:_mapView.annotations];
    }
}

- (void)dealloc {
    _mapView = nil;
    _locationButton = nil;
    _polygoneButton = nil;
}

- (NSMutableArray*)coordinates
{
    if(_coordinates == nil) _coordinates = [[NSMutableArray alloc] init];
    return _coordinates;
}


- (CanvasView*)canvasView
{
    if(_canvasView == nil) {
        _canvasView = [[CanvasView alloc] initWithFrame:self.mapView.frame];
        _canvasView.userInteractionEnabled = YES;
        _canvasView.delegate = self;
    }
    return _canvasView;
}

- (IBAction)didTouchUpInsideDrawButton:(UIButton*)sender
{
    if(self.isDrawingPolygon == NO) {
        self.isDrawingPolygon = YES;
        self.mapView.isDrawingPolygon = self.isDrawingPolygon;
        [self.polygoneButton setImage:[UIImage imageNamed:@"Delete_Sign"] forState:UIControlStateNormal];
        [self.coordinates removeAllObjects];
        for (id <MKOverlay> overlay in self.mapView.overlays) {
            if ([overlay isKindOfClass:[MKPolygon class]]) {
                [self.mapView removeOverlay:overlay];
            }
        }
        [self.view addSubview:self.canvasView];
        
    } else {
        for (id <MKOverlay> overlay in self.mapView.overlays) {
            if ([overlay isKindOfClass:[MKPolygon class]]) {
                [self.mapView removeOverlay:overlay];
            }
        }
        self.isDrawingPolygon = NO;
        self.mapView.isDrawingPolygon = self.isDrawingPolygon;
        [self.polygoneButton setImage:[UIImage imageNamed:@"polygon"] forState:UIControlStateNormal];
        self.canvasView.image = nil;
        [self.canvasView removeFromSuperview];
        [self.mapView.arrayAnnotations removeAllObjects];
        [self.mapView removeAllAnnotationAndReloadAdvert:[self.mapView setParamsForMap:self.mapView.visibleMapRect]];
       
    }
}

- (void)drawPolygone {
    if (self.isDrawingPolygon == YES) {
        NSInteger numberOfPoints = [self.coordinates count];
        
        if (numberOfPoints > 2)
        {
            CLLocationCoordinate2D points[numberOfPoints];
            for (NSInteger i = 0; i < numberOfPoints; i++) {
                points[i] = [self.coordinates[i] MKCoordinateValue];
            }
            [self.mapView addOverlay:[MKPolygon polygonWithCoordinates:points count:numberOfPoints]];
        }
        
        for (id <MKOverlay> overlay in self.mapView.overlays) {
            if ([overlay isKindOfClass:[MKPolygon class]]) {
                MKPolygon *poly = overlay;
                CLLocationCoordinate2D coords = poly.coordinate;
                NSDictionary *dict = [self.mapView setParamsForMap:poly.boundingMapRect];
                [self.mapView.arrayAnnotations removeAllObjects];
                [self.mapView removeAllAnnotationAndReloadAdvert:dict];
                NSLog(@"POLYGON COORDS: latitude: %f, longitude: %f DICTIONARY: %@", coords.latitude, coords.longitude, dict);
                break;
            }
        }
        self.canvasView.image = nil;
        [self.canvasView removeFromSuperview];
    }
}

- (void)touchesBegan:(UITouch*)touch
{
    CGPoint location = [touch locationInView:self.mapView];
    CLLocationCoordinate2D coordinate = [self.mapView convertPoint:location toCoordinateFromView:self.mapView];
    [self.coordinates addObject:[NSValue valueWithMKCoordinate:coordinate]];
}

- (void)touchesMoved:(UITouch*)touch
{
    CGPoint location = [touch locationInView:self.mapView];
    CLLocationCoordinate2D coordinate = [self.mapView convertPoint:location toCoordinateFromView:self.mapView];
    [self.coordinates addObject:[NSValue valueWithMKCoordinate:coordinate]];
}

- (void)touchesEnded:(UITouch*)touch
{
    CGPoint location = [touch locationInView:self.mapView];
    CLLocationCoordinate2D coordinate = [self.mapView convertPoint:location toCoordinateFromView:self.mapView];
    [self.coordinates addObject:[NSValue valueWithMKCoordinate:coordinate]];
    [self drawPolygone];
}

#pragma mark - setupUI

- (void)setupInterface {
    _locationButton.layer.masksToBounds = YES;
    _locationButton.layer.cornerRadius = 5;
    _locationButton.layer.borderWidth = 1.5;
    _locationButton.layer.borderColor = [UIColor colorWithRed:0.792 green:0.796 blue:0.792 alpha:1.000].CGColor;
    
    _polygoneButton.layer.masksToBounds = _locationButton.layer.masksToBounds;
    _polygoneButton.layer.cornerRadius =  _locationButton.layer.cornerRadius;
    _polygoneButton.layer.borderWidth = _locationButton.layer.borderWidth;
    _polygoneButton.layer.borderColor = _locationButton.layer.borderColor;
    
    _listAdvert.layer.masksToBounds = _locationButton.layer.masksToBounds;
    _listAdvert.layer.cornerRadius =  _locationButton.layer.cornerRadius;
    _listAdvert.layer.borderWidth = _locationButton.layer.borderWidth;
    _listAdvert.layer.borderColor = _locationButton.layer.borderColor;
    
    _viewSearch.layer.masksToBounds = _locationButton.layer.masksToBounds;
    _viewSearch.layer.cornerRadius =  _locationButton.layer.cornerRadius;
    _viewSearch.layer.borderWidth = _locationButton.layer.borderWidth;
    _viewSearch.layer.borderColor = _locationButton.layer.borderColor;
    
    _zoomIn.layer.masksToBounds = _locationButton.layer.masksToBounds;
    _zoomIn.layer.cornerRadius =  _locationButton.layer.cornerRadius;
    _zoomIn.layer.borderWidth = _locationButton.layer.borderWidth;
    _zoomIn.layer.borderColor = _locationButton.layer.borderColor;
    
    _zoomOut.layer.masksToBounds = _locationButton.layer.masksToBounds;
    _zoomOut.layer.cornerRadius =  _locationButton.layer.cornerRadius;
    _zoomOut.layer.borderWidth = _locationButton.layer.borderWidth;
    _zoomOut.layer.borderColor = _locationButton.layer.borderColor;
    
    self.searchGeoTextField.delegate = self;
}

#pragma mark - DropDown Menu Delegate

- (void)setupMenu {
    _innerView = [UIView new];
    _innerView.backgroundColor = [UIColor clearColor];
    _innerView.frame = CGRectMake(0.f, 0.f, 200.f, 200.f);
    
    UILabel *label = [UILabel new];
    label.backgroundColor = [UIColor clearColor];
    label.text = @"UIView";
    label.textColor = [UIColor blackColor];
    label.font = [UIFont fontWithName:@"AvenirNext-Medium" size:16];
    [label sizeToFit];
    label.center = CGPointMake(_innerView.frame.size.width/2, _innerView.frame.size.height/2);
    [_innerView addSubview:label];
    
    _titlesArray = @[@"Квартиры. Продажа",
                     @"Квартиры. Аренда",
                     @"Комнаты и доли. Продажа",
                     @"Комнаты. Аренда",
                     @"Дома и дачи. Продажа",
                     @"Дома и дачи. Аренда",
                     @"Коммерческая. Продажа",
                     @"Коммерческая. Аренда",
                     @"Гаражи и стоянки. Продажа",
                     @"Гаражи и стоянки. Аренда"];
    
    _categoryArray = @[@"real-estate/apartments-sale",
                       @"real-estate/rent",
                       @"real-estate/rooms-sale",
                       @"real-estate/rooms-rent",
                       @"real-estate/out-of-town",
                       @"real-estate/out-of-town-rent",
                       @"real-estate/commercial-sale",
                       @"real-estate/commercial",
                       @"real-estate/garage",
                       @"real-estate/garage-rent"];
    
    UIImage *arrowImage = [LGDrawer drawArrowWithImageSize:CGSizeMake(11.f, 8.f)
                                                      size:CGSizeMake(9.f, 6.f)
                                                    offset:CGPointZero
                                                    rotate:0.f
                                                 thickness:2.f
                                                 direction:LGDrawerDirectionBottom
                                           backgroundColor:nil
                                                     color:[UIColor blackColor]
                                                      dash:nil
                                               shadowColor:nil
                                              shadowOffset:CGPointZero
                                                shadowBlur:0.f];
    
    _titleButton = [LGButton new];
    _titleButton.adjustsAlphaWhenHighlighted = YES;
    _titleButton.backgroundColor = [UIColor clearColor];
    _titleButton.tag = 0;
    [_titleButton setTitle:_titlesArray.firstObject forState:UIControlStateNormal];
    [_titleButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_titleButton setImage:arrowImage forState:UIControlStateNormal];
    _titleButton.imagePosition = LGButtonImagePositionRight;
    _titleButton.titleLabel.font = [UIFont fontWithName:@"AvenirNext-Medium" size:16];
    [_titleButton addTarget:self action:@selector(filterAction1:) forControlEvents:UIControlEventTouchUpInside];
    [_titleButton sizeToFit];
    
    self.navigationItem.titleView = _titleButton;
    
    [self setupFilterViewsWithTransitionStyle:LGFilterViewTransitionStyleTop];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    CGFloat topInset = self.navigationController.navigationBar.frame.size.height+([UIApplication sharedApplication].isStatusBarHidden ? 0.f : [UIApplication sharedApplication].statusBarFrame.size.height);
    if ([UIDevice currentDevice].systemVersion.floatValue < 7.0) topInset = 0.f;
    [self updateFilterProperties:nil];
}

- (void)filterAction1:(LGButton *)button
{
    if (!_filterView1.isShowing)
    {
        _filterView1.selectedIndex = button.tag;
        
        [_filterView1 showInView:self.view animated:YES completionHandler:nil];
    }
    else [_filterView1 dismissAnimated:YES completionHandler:nil];
}

- (void)updateFilterProperties:(UISegmentedControl *)segmentControl
{
   
    CGFloat topInset = self.navigationController.navigationBar.frame.size.height+([UIApplication sharedApplication].isStatusBarHidden ? 0.f : [UIApplication sharedApplication].statusBarFrame.size.height);
    if ([UIDevice currentDevice].systemVersion.floatValue < 7.0) topInset = 0.f;
    
    if (_filterView1.transitionStyle == LGFilterViewTransitionStyleCenter)
    {
        _filterView1.offset = CGPointMake(0.f, topInset/2);
        _filterView1.contentInset = UIEdgeInsetsZero;
    }
    else if (_filterView1.transitionStyle == LGFilterViewTransitionStyleTop)
    {
        _filterView1.contentInset = UIEdgeInsetsMake(topInset, 0.f, 0.f, 0.f);
        _filterView1.offset = CGPointZero;
    }
}

- (void)setupFilterViewsWithTransitionStyle:(LGFilterViewTransitionStyle)style {
    __weak typeof(self) wself = self;
    
    _filterView1 = [[LGFilterView alloc] initWithTitles:_titlesArray
                                          actionHandler:^(LGFilterView *filterView, NSString *title, NSUInteger index) {
                                              if (wself) {
                                                  __strong typeof(wself) self = wself;
                                                  self.mapView.category = _categoryArray[index];
                                                  [self.mapView removeAllAnnotationAndReloadAdvert:[self.mapView setParamsForMap:self.mapView.visibleMapRect]];
                                                  [self.titleButton setTitle:title forState:UIControlStateNormal];
                                                  self.titleButton.tag = index;
                                              }
                                          }
                                          cancelHandler:nil];
    _filterView1.transitionStyle = style;
    _filterView1.numberOfLines = 0;
}

#pragma mark - IBAction
- (IBAction)toUserLocation:(UIButton *)sender {
    [self.mapView showUserLocation];
}

- (IBAction)showAdvertList:(UIButton *)sender {
    if (self.mapView.advertList.count != 0) {
        AdvertListViewController *advertLis = [self.storyboard instantiateViewControllerWithIdentifier:@"advertList"];
        if (self.mapView.advertList.count > 0) {
            advertLis.title = self.titleButton.titleLabel.text;
            advertLis.advertList = self.mapView.advertList;
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:advertLis];
            [self.navigationController presentViewController:navigationController animated:YES completion:nil];
        }
        advertLis.tableView.backgroundView = self.mapView;
    } else {
        NSLog(@"Ничего не найдено, показывать нечего");
    }
}

- (void)toDetailAdvertID:(NSString *)advertID mapStreet:(NSString *)mapStreet homeNum:(NSString *)homeNum metro:(NSString *)metro {
    DetailAdvert *detailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"detailAdvert"];
    detailVC.adversementID = advertID;
    detailVC.mapStreet = mapStreet;
    detailVC.homeNum = homeNum;
    detailVC.metro = metro;
    [self.navigationController pushViewController:detailVC animated:YES];
}

- (IBAction)zoonInAction:(UIButton *)sender {
    MKCoordinateRegion region = self.mapView.region;
    region.span.latitudeDelta /= 2.0;
    region.span.longitudeDelta /= 2.0;
    [self.mapView setRegion:region animated:YES];
}

- (IBAction)zoomOutAction:(UIButton *)sender {
    MKCoordinateRegion region = self.mapView.region;
    region.span.latitudeDelta  = MIN(region.span.latitudeDelta  * 2.0, 180.0);
    region.span.longitudeDelta = MIN(region.span.longitudeDelta * 2.0, 180.0);
    [self.mapView setRegion:region animated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier  isEqualToString: @"filters"]) {
        FiltersViewController *vc = segue.destinationViewController;
        vc.category = self.mapView.category;
    }
}

#pragma mark - MKMapViewDelegate

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id < MKOverlay >)overlay
{
    
    if (![overlay isKindOfClass:[MKPolygon class]]) {
        return nil;
    }
    MKPolygon *polygon = (MKPolygon *)overlay;
    MKPolygonRenderer *renderer = [[MKPolygonRenderer alloc] initWithPolygon:polygon];
    renderer.fillColor = [[UIColor darkGrayColor] colorWithAlphaComponent:0.2];
    renderer.strokeColor = [[UIColor colorWithRed:0.961 green:0.294 blue:0.318 alpha:1.000] colorWithAlphaComponent:0.7];
    renderer.lineWidth = 3;
    return renderer;
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    
    if([annotation isKindOfClass:[MKUserLocation class]]) {
        return nil;
    }
    
    if (![annotation conformsToProtocol:@protocol(CalloutAnnotationProtocol)])
        return nil;
    
    NSObject <CalloutAnnotationProtocol> *newAnnotation = (NSObject <CalloutAnnotationProtocol> *)annotation;
    if (newAnnotation == _calloutAnnotation)
    {
        CalloutAnnotationView *annotationView = (CalloutAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:newAnnotation.calloutType];
        if (!annotationView)
        {
            annotationView = [CalloutAnnotationView calloutWithAnnotation:newAnnotation];
        }
        else
        {
            annotationView.annotation = newAnnotation;
        }
        AdvertCalloutViewController *vc = (AdvertCalloutViewController *)annotationView.viewController;
        vc.delegate = self;
        annotationView.viewController = vc;
        annotationView.parentAnnotationView = (MKAnnotationView *)_selectedAnnotationView;
        annotationView.mapView = mapView;
        return annotationView;
    }
    GenericPinAnnotationView *annotationView = (GenericPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:GenericPinReuseIdentifier];
    if (!annotationView)
    {
        annotationView = [GenericPinAnnotationView pinViewWithAnnotation:newAnnotation];
    }
    annotationView.annotation = newAnnotation;
    return annotationView;
}



- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    [UIView animateWithDuration:0.2 delay:0.0f options:(UIViewAnimationOptionCurveLinear | UIViewAnimationOptionBeginFromCurrentState) animations:^{
        
        CLLocationCoordinate2D center = [[view annotation] coordinate];
        [self.mapView setCenterCoordinate:center animated:NO];
        
    } completion:^(BOOL finished) {
        [self performSelector:@selector(addAnnotationCalloutView:) withObject:view afterDelay:0];
//        NSLog(@"has not been interrupted : %d", finished);
    }];
}



- (void)addAnnotationCalloutView:(MKAnnotationView *)view {
    id<MKAnnotation> annotation = view.annotation;
    if (!annotation || ![view isSelected])
        return;
    if ( NO == [annotation isKindOfClass:[AdvertCalloutViewController class]] &&
        [annotation conformsToProtocol:@protocol(CalloutAnnotationProtocol)])
    {
        NSObject <CalloutAnnotationProtocol> *pinAnnotation = (NSObject <CalloutAnnotationProtocol> *)annotation;
        if (!_calloutAnnotation)
        {
            _calloutAnnotation = [[CalloutAnnotation alloc] init];
            [_calloutAnnotation copyAttributesFromAnnotation:pinAnnotation];
            [self.mapView addAnnotation:_calloutAnnotation];
        }
        _selectedAnnotationView = view;
        return;
    }
    //    [self.mapView setCenterCoordinate:annotation.coordinate animated:YES];
    _selectedAnnotationView = view;

}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {

    if ( NO == [view.annotation conformsToProtocol:@protocol(CalloutAnnotationProtocol)])
        return;
    if ([view.annotation isKindOfClass:[CalloutAnnotation class]])
        return;
    GenericPinAnnotationView *pinView = (GenericPinAnnotationView *)view;
    if (_calloutAnnotation && !pinView.preventSelectionChange)
    {
        [mapView removeAnnotation:_calloutAnnotation];
        _calloutAnnotation = nil;
    }
}

- (MKAnnotationView *)mapView:(TSClusterMapView *)mapView viewForClusterAnnotation:(id<MKAnnotation>)annotation {

    TSDemoClusteredAnnotationView *view = (TSDemoClusteredAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:NSStringFromClass([TSDemoClusteredAnnotationView class])];
    if (!view) {
        view = [[TSDemoClusteredAnnotationView alloc] initWithAnnotation:annotation
                                                         reuseIdentifier:NSStringFromClass([TSDemoClusteredAnnotationView class])];
    }
    
    return view;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField.text.length > 0) {
        [self getLocationFromAddressString:textField.text];
    }
    [textField resignFirstResponder];
    return YES;
}

- (void) getLocationFromAddressString: (NSString*) addressStr {
    
    double latitude = 0, longitude = 0;
    
    NSString *esc_addr =  [addressStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    
    if (result) {
        
        NSScanner *scanner = [NSScanner scannerWithString:result];
        
        if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
            [scanner scanDouble:&latitude];
            if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
                [scanner scanDouble:&longitude];
            }
        }
    }
    CLLocationCoordinate2D center;
    center.latitude=latitude;
    center.longitude = longitude;
    
    MKCoordinateRegion region;
    region.center = center;
    MKCoordinateSpan span;
    region.span = span;

    [self.mapView setRegion:region animated:YES];

}


@end
