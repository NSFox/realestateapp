//
//  AdvertNoteCallout.m
//  realestateapp
//
//  Created by Юрий Дурнев on 01.09.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "AdvertNoteCallout.h"
#import "AdvertCalloutViewController.h"

@implementation AdvertNoteCallout

- (instancetype)initWithPhoto:(AdvertNote *)photo
{
    self = [super init];
    if (self)
    {
        _advertNote = photo;
    }
    return self;
}

- (UIViewController *)calloutCell
{
    AdvertCalloutViewController * viewController = [AdvertCalloutViewController new];
    viewController.advert = _advertNote;
    return viewController;
}

@end
