//
//  KeyWords.m
//  realestateapp
//
//  Created by Юрий Дурнев on 28.09.15.
//  Copyright © 2015 irr. All rights reserved.
//

#import "KeyWords.h"

NSString * const KeyWordsCell = @"KeyWordsCell";

@implementation KeyWords
+ (void)load {
    [XLFormViewController.cellClassesForRowDescriptorTypes setObject:NSStringFromClass([KeyWords class])  forKey:KeyWordsCell];
}

- (void)configure {
    [super configure];
}

- (void)update {
    [super update];
}

+ (CGFloat)formDescriptorCellHeightForRowDescriptor:(XLFormRowDescriptor *)rowDescriptor {
    return 44;
}

@end
