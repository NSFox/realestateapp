//
//  XLContactCell.h
//  realestateapp
//
//  Created by Юрий Дурнев on 28.07.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "XLFormBaseCell.h"
extern NSString * const XLContactText;

@interface XLContactCell : XLFormBaseCell <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextField *textFieldContact;

@end
