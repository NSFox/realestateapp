//
//  FiltersModel.m
//  realestateapp
//
//  Created by Юрий Дурнев on 09.09.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "FiltersModel.h"


@implementation ValueModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"value": @"value",
             @"title": @"title",
             @"idValue" : @"id",
             @"filterName": @"filter_name",
             @"checked": @"checked",
             @"items": @"items"
             };
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue error:(NSError **)error {
    self = [super initWithDictionary:dictionaryValue error:error];
    if (self == nil) return nil;
    //    _customFields = [CustomFields deserializeAppInfosFromJSON:dictionaryValue[@"customFields"]];
    
    return self;
}

+ (NSArray *)deserializeValueFieldsJSON:(NSArray *)appInfosJSON {
    NSError *error;
    
    NSArray *arrayModel = [MTLJSONAdapter modelsOfClass:[ValueModel class] fromJSONArray:appInfosJSON error:&error];
    if (error) {
        NSLog(@"Couldn't convert app infos JSON to DetailAdvertModel models: %@", error);
        return nil;
    }
    
    return arrayModel;
}

@end

@implementation ElementsModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"type": @"type",
             @"title": @"title",
             @"name" : @"name",
             @"dictionary": @"dictionary",
             @"selectedItemsCount": @"selected_items_count",
             @"values": @"values"
             };
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue error:(NSError **)error {
    self = [super initWithDictionary:dictionaryValue error:error];
    if (self == nil) return nil;
        _values = [ValueModel deserializeValueFieldsJSON:dictionaryValue[@"values"]];
    
    return self;
}

+ (NSArray *)deserializeElementsFieldsJSON:(NSArray *)appInfosJSON {
    NSError *error;
    
    NSArray *arrayModel = [MTLJSONAdapter modelsOfClass:[ElementsModel class] fromJSONArray:appInfosJSON error:&error];
    if (error) {
        NSLog(@"Couldn't convert app infos JSON to DetailAdvertModel models: %@", error);
        return nil;
    }
    
    return arrayModel;
}

@end

@implementation FiltersModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"type": @"type",
             @"title": @"title",
             @"showMode" : @"show_mode",
             @"elements": @"elements"
             };
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue error:(NSError **)error {
    self = [super initWithDictionary:dictionaryValue error:error];
    if (self == nil) return nil;
    _elements = [ElementsModel deserializeElementsFieldsJSON:dictionaryValue[@"elements"]];
    
    return self;
}

+ (NSArray *)deserializeFiltersFieldsJSON:(NSArray *)appInfosJSON {
    NSError *error;
    
    NSArray *arrayModel = [MTLJSONAdapter modelsOfClass:[FiltersModel class] fromJSONArray:appInfosJSON error:&error];
    if (error) {
        NSLog(@"Couldn't convert app infos JSON to DetailAdvertModel models: %@", error);
        return nil;
    }
    
    return arrayModel;
}

@end
