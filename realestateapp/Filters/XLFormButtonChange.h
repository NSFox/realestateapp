//
//  XLFormButtonChange.h
//  realestateapp
//
//  Created by Юрий Дурнев on 29.06.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "XLFormBaseCell.h"

extern NSString * const XLFormRowDescriptorTypeRate;

@interface XLFormButtonChange : XLFormBaseCell
@property (weak, nonatomic) IBOutlet UILabel *rateTitle;
@property (weak, nonatomic) IBOutlet UIButton *ratingView;
@end
