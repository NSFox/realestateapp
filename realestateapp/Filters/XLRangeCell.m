//
//  XLRangeCell.m
//  realestateapp
//
//  Created by NSFox on 03.06.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "XLRangeCell.h"

NSString * const XLFormRangeText = @"XLFormRangeText";

@interface XLRangeCell () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UITextField *atFields;
@property (weak, nonatomic) IBOutlet UITextField *toFields;

@end

@implementation XLRangeCell

+ (void)load {
    [XLFormViewController.cellClassesForRowDescriptorTypes setObject:NSStringFromClass([XLRangeCell class])  forKey:XLFormRangeText];
}


- (void)configure {
    [super configure];
    self.atFields.delegate = self;
    self.toFields.delegate = self;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    textField.text = [self priceFormatter:textField.text];
    [self.formViewController textFieldDidEndEditing:textField];
}

- (NSString *)priceFormatter:(NSString *)price {
    NSNumberFormatter *frmtr;
    if (frmtr == nil) {
        frmtr = [[NSNumberFormatter alloc] init];
    }
    
    [frmtr setGroupingSize:3];
    [frmtr setGroupingSeparator:@" "];
    [frmtr setUsesGroupingSeparator:YES];
    NSString *commaString = [frmtr stringFromNumber:[NSNumber numberWithInteger:price.integerValue]];
    return commaString;
}

- (void)update {
    [super update];
}

+ (CGFloat)formDescriptorCellHeightForRowDescriptor:(XLFormRowDescriptor *)rowDescriptor {
    return 79;
}

@end
