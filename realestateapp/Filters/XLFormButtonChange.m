//
//  XLFormButtonChange.m
//  realestateapp
//
//  Created by Юрий Дурнев on 29.06.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "XLFormButtonChange.h"
NSString * const XLFormRowDescriptorTypeRate = @"XLFormRowDescriptorTypeRate";

@implementation XLFormButtonChange

+(void)load
{
    [XLFormViewController.cellClassesForRowDescriptorTypes setObject:NSStringFromClass([XLFormButtonChange class]) forKey:XLFormRowDescriptorTypeRate];
}

- (void)configure
{
    [super configure];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.ratingView addTarget:self action:@selector(rateChanged) forControlEvents:UIControlEventTouchUpInside];
}

- (void)update
{
    [super update];
    
    self.rateTitle.text = self.rowDescriptor.title;
    self.ratingView.backgroundColor = [UIColor redColor];
    [self.ratingView setTitle:self.rowDescriptor.title forState:UIControlStateNormal];
}

-(void)rateChanged
{
    NSLog(@"Tap");
}

+(CGFloat)formDescriptorCellHeightForRowDescriptor:(XLFormRowDescriptor *)rowDescriptor {
    return 44;
}

@end
