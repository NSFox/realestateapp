//
//  FiltersViewController.h
//  realestateapp
//
//  Created by NSFox on 15.05.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XLFormRoomCell.h"
#import "XLRangeCell.h"
#import "XLFormButtonChange.h"
#import "KeyWords.h"
#import "XLSelectorOptionsCell.h"

@interface FiltersViewController : XLFormViewController
@property (strong, nonatomic) NSString *category;
@end
