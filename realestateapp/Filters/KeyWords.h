//
//  KeyWords.h
//  realestateapp
//
//  Created by Юрий Дурнев on 28.09.15.
//  Copyright © 2015 irr. All rights reserved.
//

#import <XLForm/XLForm.h>
extern NSString * const KeyWordsCell;

@interface KeyWords : XLFormBaseCell
@property (weak, nonatomic) IBOutlet UITextField *keyFields;
@end
