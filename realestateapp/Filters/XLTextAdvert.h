//
//  XLTextAdvert.h
//  realestateapp
//
//  Created by Юрий Дурнев on 28.07.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "XLFormBaseCell.h"
extern NSString * const XLTextAdvertCell;

@interface XLTextAdvert : XLFormBaseCell <UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *titleText;
@property (weak, nonatomic) IBOutlet UITextView *advertText;


@end
