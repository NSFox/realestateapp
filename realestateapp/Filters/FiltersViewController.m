//
//  FiltersViewController.m
//  realestateapp
//
//  Created by NSFox on 15.05.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "FiltersViewController.h"
#import "FiltersModel.h"

@interface FiltersViewController ()
@property (strong, nonatomic) NSArray *filters;
@property (strong, nonatomic) NSArray *extend;
@end

@implementation FiltersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeForm];
    self.title = @"Фильтр";
    
    [[APIClient sharedClient] getFilters:@{@"category":self.category} withSuccessBlock:^(NSURLSessionDataTask *task, id responseObject) {
        self.filters = [FiltersModel deserializeFiltersFieldsJSON:responseObject[@"default"]];
        self.extend = [FiltersModel deserializeFiltersFieldsJSON:responseObject[@"extended"]];
        [self addCustomField:self.filters];
        [self addCustomField:self.extend];
        [self addConfirmButton];
    } andFailBlock:^(NSURLSessionDataTask *task, NSError *error) {
        
    }];
}

- (void)initializeForm {
    XLFormDescriptor *form = [XLFormDescriptor formDescriptor];
    self.form = form;
}

- (void)addConfirmButton {
    XLFormRowDescriptor * buttonLeftAlignedRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"send" rowType:XLFormRowDescriptorTypeButton title:@"Применить фильтр"];
    [buttonLeftAlignedRow.cellConfigAtConfigure setObject:[UIColor colorWithRed:0.961 green:0.294 blue:0.318 alpha:1.000] forKey:@"backgroundColor"];
    [buttonLeftAlignedRow.cellConfig setObject:[UIColor whiteColor] forKey:@"textLabel.textColor"];
    [buttonLeftAlignedRow.cellConfig setObject:@(NSTextAlignmentCenter) forKey:@"textLabel.textAlignment"];
    [buttonLeftAlignedRow.cellConfig setObject:[UIFont fontWithName:@"AvenirNext-Medium" size:14.0] forKey:@"detailTextLabel.font"];

    __typeof(self) __weak weakSelf = self;
    buttonLeftAlignedRow.action.formBlock = ^(XLFormRowDescriptor *sender){

        [weakSelf deselectFormRow:sender];
    };
    
    [self.form.formSections.lastObject addFormRow:buttonLeftAlignedRow];
}

- (void)addCustomField:(NSArray *)field {
    XLFormSectionDescriptor *section = [XLFormSectionDescriptor formSection];
    [self.form addFormSection:section];
    
    for (FiltersModel *custom in field) {
        for (ElementsModel *elements in custom.elements) {
            
            XLFormRowDescriptor *row = [XLFormRowDescriptor formRowDescriptorWithTag:elements.title rowType:[self typeRow:elements.type title:elements.title]];
            [self rowConfig:row withTitle:elements.title andValuePlaceholder:@"Не выбрано"];
            //                row.action.viewControllerClass = [RLSelectionNavigationViewController class];
            NSMutableArray *valueArray = [NSMutableArray new];
            for (ValueModel *value in elements.values) {
                [valueArray addObject:value.title];
            }
            row.selectorOptions = valueArray;
            [section addFormRow:row];
        }
    }
}

- (void)rowConfig:(XLFormRowDescriptor *)row withTitle:(NSString *)title andValuePlaceholder:(NSString *)placeholder {
    [row.cellConfig setObject:[UIFont fontWithName:@"AvenirNext-Medium" size:12.0] forKey:@"textLabel.font"];
    [row.cellConfig setObject:[UIColor grayColor] forKey:@"textLabel.textColor"];
    [row.cellConfig setObject:[UIFont fontWithName:@"AvenirNext-Medium" size:14.0] forKey:@"detailTextLabel.font"];
    [row.cellConfig setObject:[UIColor colorWithRed:0.961 green:0.294 blue:0.318 alpha:1.000] forKey:@"detailTextLabel.textColor"];
    row.title = title.uppercaseString;
    
    if ([row.rowType isEqualToString:@"XLFormRangeText"]) {
        [row.cellConfig setObject:title.uppercaseString forKey:@"title.text"];
    }
}

- (NSString *)typeRow:(NSString *)type title:(NSString *)title {
    if ([type isEqualToString:@"string"]) {
        return XLFormRowDescriptorTypeText;
    }
    
    if ([type isEqualToString:@"checkbox"] && [title isEqualToString:@"Комнат в квартире"]) {
        return XLFormRowDescriptorTypeWeekDays;
    } else if ([type isEqualToString:@"checkbox"] && ![title isEqualToString:@"Комнат в квартире"]) {
        return XLFormRowDescriptorTypeBooleanSwitch;
    }
    
    if ([type isEqualToString:@"range"]) {
        return XLFormRangeText;
    }
    
    if ([type isEqualToString:@"bool"]) {
        return XLFormRowDescriptorTypeBooleanSwitch;
    }
    
    if ([type isEqualToString:@"select"] || [type isEqualToString:@"locality"] || [type isEqualToString:@"metro"]) {
        return XLFormRowDescriptorTypeSelectorPush;
    }
    
    if ([type isEqualToString:@"text"]) {
        return KeyWordsCell;
    }
    
    if ([type isEqualToString:@"price"]) {
        return XLFormRangeText;
    }
    
    return XLFormRowDescriptorTypeSelectorPush;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }

    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

@end
