//
//  XLPhotoChangeCell.m
//  realestateapp
//
//  Created by Юрий Дурнев on 27.07.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "XLPhotoChangeCell.h"
#import "LessBoringFlowLayout.h"
#import "AddCell.h"
#import "ColorNameCell.h"
#import "UIImage-JTColor.h"

NSString * const XLPhotoChangeCollectionView = @"XLPhotoChangeCollectionView";

@interface XLPhotoChangeCell () {
    NSIndexPath *indexPathImage;
}
@property (strong, nonatomic) NSMutableArray *photoView;
@property (strong, nonatomic) NSMutableArray *sectionedNames;
@end

@implementation XLPhotoChangeCell

+ (void)load {
    [XLFormViewController.cellClassesForRowDescriptorTypes setObject:NSStringFromClass([XLPhotoChangeCell class])  forKey:XLPhotoChangeCollectionView];
}

- (void)configure {
    [super configure];
    LessBoringFlowLayout *layout = [[LessBoringFlowLayout alloc] init];
    layout.minimumInteritemSpacing = 10.f;
    layout.minimumLineSpacing = 10.f;
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.sectionInset = UIEdgeInsetsMake(0.f, 10.f, 0.f, 0.f);
    self.collectionView.collectionViewLayout = layout;
    layout.itemSize = CGSizeMake(120, 120);
    [layout makeBoring];
    
    self.sectionedNames = [@[[NSMutableArray array]] mutableCopy];
    self.photoView = [NSMutableArray new];
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([AddCell class]) bundle:nil]
          forCellWithReuseIdentifier:NSStringFromClass([AddCell class])];
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([ColorNameCell class]) bundle:nil]
          forCellWithReuseIdentifier:NSStringFromClass([ColorNameCell class])];
}

- (void)update {
    [super update];
}

+ (CGFloat)formDescriptorCellHeightForRowDescriptor:(XLFormRowDescriptor *)rowDescriptor {
    return 130;
}

//- (void)awakeFromNib {
//    
//
//}

#pragma mark - UICollectionView

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
        UICollectionViewCell *cell;
        NSArray *colorNames = self.sectionedNames[indexPath.section];
        if (indexPath.row == colorNames.count) {
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([AddCell class])
                                                             forIndexPath:indexPath];
        } else {
            ColorNameCell *cnCell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([ColorNameCell class])
                                                                              forIndexPath:indexPath];
            if (_photoView.count !=0) {
                cnCell.imageViewAdvert.image = _photoView[(NSUInteger) (indexPath.row - 1)];
            }
            cell = cnCell;
        }
        return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
  
        NSArray *colorNames = self.sectionedNames[(NSUInteger) indexPath.section];
        
        if (indexPath.item == colorNames.count) {
            if (self.photoView.count <=10) {
                indexPathImage = indexPath;
                
                AHKActionSheet *actionSheet = [[AHKActionSheet alloc] init];
                [actionSheet addButtonWithTitle:@"Сделать фото" type:AHKActionSheetButtonTypeDefault handler:^(AHKActionSheet *as) {
                    TGCameraNavigationController *navigationController = [TGCameraNavigationController newWithCameraDelegate:self];
                    [self.viewContoller presentViewController:navigationController animated:YES completion:nil];
                }];
                [actionSheet addButtonWithTitle:@"Выбрать из Альбома" type:AHKActionSheetButtonTypeDefault handler:^(AHKActionSheet *as) {
                        UIImagePickerController *pickerController = [TGAlbum imagePickerControllerWithDelegate:self];
                        [self.viewContoller presentViewController:pickerController animated:YES completion:nil];
                }];
                actionSheet.buttonTextCenteringEnabled = @YES;
                [actionSheet show];
            } else {
                [[[UIAlertView alloc] initWithTitle:@"Внимание!"
                                            message:@"Добавлено максимальное количество фотографий"
                                           delegate:self
                                  cancelButtonTitle:@"ОК"
                                  otherButtonTitles:nil, nil] show];
            }
        } else {
            [self deleteItemAtIndexPath:indexPath];
        }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
        NSMutableArray *sectionColorNames = self.sectionedNames[(NSUInteger) section];
        if (self.photoView.count == 0) {
            [UIView animateWithDuration:0.4f animations:^{
                self.labelMaxPhoto.alpha = 1.0f;
            }];
            
        } else {
            [UIView animateWithDuration:0.4f animations:^{
                self.labelMaxPhoto.alpha = 0.0f;
            }];
        }
        return self.sectionedNames.count;
}

#pragma mark - Object insert/remove

- (void)addNewItemInSection:(NSUInteger)section {
    
    NSArray *newData = [[NSArray alloc] initWithObjects:@"otherData", nil];
    [self.collectionView performBatchUpdates:^{
        NSUInteger resultsSize = [self.sectionedNames count]; //data is the previous array of data
        [self.sectionedNames addObjectsFromArray:newData];
        NSMutableArray *arrayWithIndexPaths = [NSMutableArray array];
        
        for (NSUInteger i = resultsSize; i < resultsSize + newData.count; i++) {
            [arrayWithIndexPaths addObject:[NSIndexPath indexPathForRow:i
                                                              inSection:0]];
        }
        [self.collectionView insertItemsAtIndexPaths:arrayWithIndexPaths];
    } completion:nil];
    self.rowDescriptor.value = self.photoView;
}

- (void)deleteItemAtIndexPath:(NSIndexPath *)indexPath {
    NSMutableArray *colorNames = self.sectionedNames;
    [colorNames removeObjectAtIndex:(NSUInteger) indexPath.row];
    [self.photoView removeObjectAtIndex:(NSUInteger) indexPath.row - 1];
    [self.collectionView deleteItemsAtIndexPaths:@[indexPath]];
    self.rowDescriptor.value = self.photoView;
}

#pragma mark -
#pragma mark - TGCameraDelegate

- (void)cameraDidTakePhoto:(UIImage *)image {
    [self.viewContoller dismissViewControllerAnimated:YES completion:nil];
    [self.photoView addObject:image];
    [self addNewItemInSection:indexPathImage.section];
}

- (void)cameraDidCancel {
    [self.viewContoller dismissViewControllerAnimated:YES completion:nil];
}

- (void)cameraDidSelectAlbumPhoto:(UIImage *)image {
    
}

- (void)cameraWillTakePhoto {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

#pragma mark -
#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [self.viewContoller dismissViewControllerAnimated:YES completion:nil];
    [self.photoView addObject:[TGAlbum imageWithMediaInfo:info]];
    [self addNewItemInSection:indexPathImage.section];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self.viewContoller dismissViewControllerAnimated:YES completion:nil];
}


@end
