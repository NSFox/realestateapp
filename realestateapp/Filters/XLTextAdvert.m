//
//  XLTextAdvert.m
//  realestateapp
//
//  Created by Юрий Дурнев on 28.07.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "XLTextAdvert.h"

NSString * const XLTextAdvertCell = @"XLTextAdvertCell";

@implementation XLTextAdvert

+ (void)load {
    [XLFormViewController.cellClassesForRowDescriptorTypes setObject:NSStringFromClass([XLTextAdvert class])  forKey:XLTextAdvertCell];
}

- (void)configure {
    [super configure];
}

- (void)update {
    [super update];
}

+ (CGFloat)formDescriptorCellHeightForRowDescriptor:(XLFormRowDescriptor *)rowDescriptor {
    return 160;
}

- (void)awakeFromNib {
    self.advertText.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.advertText.layer.borderWidth = 0.5;
    self.advertText.layer.cornerRadius = 5;
    self.advertText.delegate = self;
}

#pragma mark - UITextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [self.formViewController textViewDidBeginEditing:textView];
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    [self textViewDidChange:textView];
}

- (void)textViewDidChange:(UITextView *)textView
{
    if (self.advertText == textView) {
        if ([self.advertText.text length] > 0) {
            self.rowDescriptor.value = self.advertText.text;
        } else {
            self.rowDescriptor.value = nil;
        }
    }
}

@end
