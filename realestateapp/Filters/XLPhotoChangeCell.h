//
//  XLPhotoChangeCell.h
//  realestateapp
//
//  Created by Юрий Дурнев on 27.07.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "XLFormBaseCell.h"

extern NSString * const XLPhotoChangeCollectionView;

@protocol XLPhotoChangeDelegate <NSObject>
- (void)loadCameraPhoto;
- (void)loadImagePickerPhoto;
@end

@interface XLPhotoChangeCell : XLFormBaseCell <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, TGCameraDelegate, UINavigationControllerDelegate>

@property (weak, nonatomic) id<XLPhotoChangeDelegate> delegate;
@property (strong, nonatomic) UIViewController *viewContoller;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *labelMaxPhoto;

@end
