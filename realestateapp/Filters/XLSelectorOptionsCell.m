//
//  XLSelectorOptionsCell.m
//  realestateapp
//
//  Created by Юрий Дурнев on 22.07.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "XLSelectorOptionsCell.h"

NSString * const XLSelectorOptions = @"XLSelectorOptions";

@interface XLSelectorOptionsCell ()
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *valueSelected;

@end

@implementation XLSelectorOptionsCell

+ (void)load {
    [XLFormViewController.cellClassesForRowDescriptorTypes setObject:NSStringFromClass([XLSelectorOptionsCell class])  forKey:XLSelectorOptions];
}

- (void)configure {
    [super configure];
//    self.title.text = self.rowDescriptor.title;
    self.rowDescriptor.selectorTitle = self.textLabel.text;
    self.valueSelected.text = @"Не выбрано";
}

- (void)update {
    [super update];
//    self.title.text = self.rowDescriptor.title;
    self.rowDescriptor.selectorTitle = self.textLabel.text;
    self.valueSelected.text = self.rowDescriptor.value == nil ? @"Не выбрано" : self.rowDescriptor.value;
}

+ (CGFloat)formDescriptorCellHeightForRowDescriptor:(XLFormRowDescriptor *)rowDescriptor {
    return 44;
}

- (void)formDescriptorCellDidSelectedWithFormController:(XLFormViewController *)controller {
//    XLFormOptionsViewController * optionsViewController = [[XLFormOptionsViewController alloc] initWithStyle:UITableViewStyleGrouped titleHeaderSection:nil titleFooterSection:nil];
//    optionsViewController.rowDescriptor = self.rowDescriptor;
//    optionsViewController.title = self.rowDescriptor.selectorTitle;
//  
//    [controller.navigationController pushViewController:optionsViewController animated:YES];
    
}

@end
