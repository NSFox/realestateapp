//
//  FiltersModel.h
//  realestateapp
//
//  Created by Юрий Дурнев on 09.09.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "MTLModel.h"



@interface ValueModel : MTLModel <MTLJSONSerializing>
@property (copy, readonly, nonatomic) NSArray *items;
@property (copy, readonly, nonatomic) NSString *title;
@property (copy, readonly, nonatomic) NSString *value;
@property (copy, readonly, nonatomic) NSString *idValue;
@property (copy, readonly, nonatomic) NSString *filterName;
@property (copy, readonly, nonatomic) NSString *checked;

+ (NSArray *)deserializeValueFieldsJSON:(NSArray *)appInfosJSON;
@end

@interface ElementsModel : MTLModel <MTLJSONSerializing>
@property (copy, readonly, nonatomic) NSArray *values;
@property (copy, readonly, nonatomic) NSString *title;
@property (copy, readonly, nonatomic) NSString *name;
@property (copy, readonly, nonatomic) NSString *type;
@property (copy, readonly, nonatomic) NSString *dictionary;
@property (assign, readonly, nonatomic) NSInteger selectedItemsCount;

+ (NSArray *)deserializeElementsFieldsJSON:(NSArray *)appInfosJSON;
@end

@interface FiltersModel : MTLModel <MTLJSONSerializing>
@property (copy, readonly, nonatomic) NSArray *elements;
@property (copy, readonly, nonatomic) NSString *title;
@property (copy, readonly, nonatomic) NSString *showMode;
@property (copy, readonly, nonatomic) NSString *type;

+ (NSArray *)deserializeFiltersFieldsJSON:(NSArray *)appInfosJSON;
@end
