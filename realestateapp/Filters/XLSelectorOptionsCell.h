//
//  XLSelectorOptionsCell.h
//  realestateapp
//
//  Created by Юрий Дурнев on 22.07.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "XLFormBaseCell.h"
extern NSString * const XLSelectorOptions;

@interface XLSelectorOptionsCell : XLFormSelectorCell
@end
