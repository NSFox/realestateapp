//
//  XLContactCell.m
//  realestateapp
//
//  Created by Юрий Дурнев on 28.07.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "XLContactCell.h"
NSString * const XLContactText = @"XLContactText";

@implementation XLContactCell

+ (void)load {
    [XLFormViewController.cellClassesForRowDescriptorTypes setObject:NSStringFromClass([XLContactCell class])  forKey:XLContactText];
}

- (void)configure {
    [super configure];
    self.textFieldContact.delegate = self;
}

- (void)update {
    [super update];
}

+ (CGFloat)formDescriptorCellHeightForRowDescriptor:(XLFormRowDescriptor *)rowDescriptor {
    return 70;
}

-(BOOL)formDescriptorCellCanBecomeFirstResponder
{
    return !self.rowDescriptor.isDisabled;
}

-(BOOL)formDescriptorCellBecomeFirstResponder
{
    return [self.textFieldContact becomeFirstResponder];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return [self.formViewController textFieldShouldClear:textField];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return [self.formViewController textFieldShouldReturn:textField];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return [self.formViewController textFieldShouldBeginEditing:textField];
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return [self.formViewController textFieldShouldEndEditing:textField];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    return [self.formViewController textField:textField shouldChangeCharactersInRange:range replacementString:string];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self.formViewController textFieldDidBeginEditing:textField];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self textFieldDidChange:textField];
    [self.formViewController textFieldDidEndEditing:textField];
}

-(void)setReturnKeyType:(UIReturnKeyType)returnKeyType
{
    self.textFieldContact.returnKeyType = returnKeyType;
}

-(UIReturnKeyType)returnKeyType
{
    return self.textFieldContact.returnKeyType;
}

- (void)textFieldDidChange:(UITextField *)textField
{
    if (self.textFieldContact == textField) {
        if ([self.textFieldContact.text length] > 0) {
            self.rowDescriptor.value = self.textFieldContact.text;
        } else {
            self.rowDescriptor.value = nil;
        }
    }
}

@end
