//
//  SupportViewController.m
//  realestateapp
//
//  Created by Юрий Дурнев on 23/03/15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "SupportViewController.h"

@interface SupportViewController ()
@property (weak, nonatomic) IBOutlet UIButton *supportMailSend;
@property (weak, nonatomic) IBOutlet UIButton *lisenceButton;

@end

@implementation SupportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _supportMailSend.layer.masksToBounds = YES;
    _supportMailSend.layer.cornerRadius = 5;
    
    _lisenceButton.layer.masksToBounds = YES;
    _lisenceButton.layer.cornerRadius = 5;
    _lisenceButton.layer.borderWidth = 1;
    _lisenceButton.layer.borderColor = [UIColor colorWithRed:0.965 green:0.294 blue:0.318 alpha:1.000].CGColor;
    self.title = @"О приложении";
}

- (IBAction)sendMail:(UIButton *)sender {
    
}

- (IBAction)viewAbout:(UIButton *)sender {
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
