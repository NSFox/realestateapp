//
//  AdvertListViewController.m
//  realestateapp
//
//  Created by Юрий Дурнев on 30/03/15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "AdvertListViewController.h"
#import "MapsViewController.h"
#import "UIViewController+MaterialDesign.h"
#import "ListCell.h"
#import "MAdvert.h"
#import "DetailAdvert.h"

static NSString * const cellIdentifier = @"ListCell";

@interface AdvertListViewController () <UITableViewDelegate, UITableViewDataSource>

@end

@implementation AdvertListViewController

#pragma mark - LifeCircle

- (void)viewDidLoad {
    [super viewDidLoad];
//    [self.tableView reloadData];
//    self.title = @"Список";
    UIBarButtonItem *backItems = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon-chevron-right"] style:UIBarButtonItemStylePlain target:self action:@selector(backToMaps)];
    self.navigationItem.leftBarButtonItem = backItems;
}

- (void)backToMaps {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    DetailAdvert *detailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"detailAdvert"];
  
    if (self.advertList.count != 0) {
         detailVC.adversementID = [self.advertList[indexPath.row] adversementID];
         detailVC.mapStreet = [self.advertList[indexPath.row] mapStreet];
         detailVC.homeNum = [self.advertList[indexPath.row] mapHouseNr];
         detailVC.metro = [self.advertList[indexPath.row] metro];
         [self.navigationController pushViewController:detailVC animated:YES];
    }
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ListCell *cell = (ListCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    if (self.advertList.count > 0) {
        MAdvert *advert = _advertList[indexPath.row];
        if (advert) {
            cell.advertTitle.text = advert.metro == nil ? @"Не указано" : advert.metro;
            cell.advertPrice.text = [self priceFormatter:[NSNumber numberWithInteger:advert.price]];
            cell.advertPlace.text = advert.mapStreet == nil && advert.mapHouseNr == nil ? @"Не указано":
            [NSString stringWithFormat:@"%@ %@", advert.mapStreet, advert.mapHouseNr == nil ? @"" : advert.mapHouseNr];
            
            cell.advertDescription.text = advert.title == nil ? @"Не указано":advert.title;
            
            if (![advert.isPremium isEqualToString:@""]) {
                if (advert.isPremium) {
                    cell.advertPriceBackground.backgroundColor = [UIColor colorWithRed:0.735 green:0.738 blue:0.004 alpha:1.000];
                }
            }
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                if (advert.images.count != 0) {
                    NSString *imageURL = advert.images[0][@"mobile"];
                    __weak UIImageView *weakSelf = cell.advertImage;
                    [cell.advertImage sd_setImageWithURL:[NSURL URLWithString:imageURL]
                                        placeholderImage:[UIImage imageNamed:@"placeholder"]
                                                 options:SDWebImageRetryFailed
                                               completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                   weakSelf.alpha = 0.0;
                                                   [UIView animateWithDuration:0.35f
                                                                    animations:^{
                                                                        weakSelf.alpha = 1.0;
                                                                    }];
                                                   
                                               }];
                }
                
            });
        }
    }
 
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.advertList.count;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (NSString *)priceFormatter:(NSNumber *)price {
    NSNumberFormatter *frmtr = [[NSNumberFormatter alloc] init];
    [frmtr setGroupingSize:3];
    [frmtr setGroupingSeparator:@" "];
    [frmtr setUsesGroupingSeparator:YES];
    NSString *commaString = [frmtr stringFromNumber:price];
    if ([commaString isEqualToString:@"0"]) {
        commaString = @"";
        return commaString;
    } else {
        NSString *currency = @"р";
        NSString *pruceWithCurrency = [NSString stringWithFormat:@"%@ %@", commaString, currency];
        return pruceWithCurrency;
    }
}

@end
