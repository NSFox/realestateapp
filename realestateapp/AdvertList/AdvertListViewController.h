//
//  AdvertListViewController.h
//  realestateapp
//
//  Created by Юрий Дурнев on 30/03/15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "ViewController.h"

@interface AdvertListViewController : UIViewController

@property (nonatomic, strong) NSArray *advertList;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (NSString *)priceFormatter:(NSNumber *)price;
@end
