//
//  ListCell.h
//  realestateapp
//
//  Created by Юрий Дурнев on 30/03/15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *advertImage;
@property (weak, nonatomic) IBOutlet UILabel *advertPrice;
@property (weak, nonatomic) IBOutlet UILabel *advertTitle;
@property (weak, nonatomic) IBOutlet UILabel *advertDescription;
@property (weak, nonatomic) IBOutlet UILabel *advertPlace;
@property (weak, nonatomic) IBOutlet UIView *advertPriceBackground;

@end
