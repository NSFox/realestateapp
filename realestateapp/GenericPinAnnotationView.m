//
//  GenericPinAnnotationView.m
//  Created by Gregory Combs on 11/30/11.
//
//  based on work at https://github.com/grgcombs/MultiRowCalloutAnnotationView
//
//  This work is licensed under the Creative Commons Attribution 3.0 Unported License. 
//  To view a copy of this license, visit http://creativecommons.org/licenses/by/3.0/
//  or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
//
//

#import "GenericPinAnnotationView.h"
#import "MapCallout.h"

NSString* const GenericPinReuseIdentifier = @"GenericPinReuse";

@implementation GenericPinAnnotationView

+ (instancetype)pinViewWithAnnotation:(NSObject <MKAnnotation> *)annotation
{
    return [[GenericPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:GenericPinReuseIdentifier];
}

- (instancetype)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self)
    {
        [self clusteringAnimation];
        self.label = [[UILabel alloc] initWithFrame:self.frame];
        self.label.textAlignment = NSTextAlignmentCenter;
        self.label.font = [UIFont systemFontOfSize:8];
        self.label.textColor = [UIColor whiteColor];
        self.label.center = CGPointMake(self.image.size.width/2, self.image.size.height*.43);
        self.centerOffset = CGPointMake(0, -self.frame.size.height/2);
        
        [self addSubview:self.label];
       
        self.canShowCallout = NO;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    if (!_preventSelectionChange)
    {
        [super setSelected:selected animated: animated];
    }
}

- (void)clusteringAnimation {
    MapCallout *clusterAnnotation = (MapCallout *)self.annotation;
    self.label.text = [self numberLabelText:clusterAnnotation];
    self.image = [self imageForCallOut:clusterAnnotation];
}

- (UIImage *)imageForCallOut:(MapCallout *)callout {
    NSUInteger roomsNum = callout.calloutData.advertNote.advert.rooms.integerValue;
    
    switch (roomsNum) {
        case 1: return [UIImage imageNamed:@"one"];
            break;
        case 2: return [UIImage imageNamed:@"two"];
            break;
        case 3: return [UIImage imageNamed:@"three"];
            break;
        case 4: return [UIImage imageNamed:@"four"];
            break;
        case 5: return [UIImage imageNamed:@"five"];
            break;
        default: 
            break;
    }
    
    if (roomsNum > 5) {
        return [UIImage imageNamed:@"five"];
    }
    return nil;
}

- (NSString *)numberLabelText:(MapCallout *)callout {
    NSUInteger count = callout.calloutData.advertNote.advert.price;
    if (!count) {
        return nil;
    }
    
    if (count >= 1000) {
        float rounded;
        if (count <= 10000) {
            rounded = ceilf(count/100)/10;
            return [NSString stringWithFormat:@"%.1fk", rounded];
        }
        if (count >= 10000) {
            rounded = ceilf(count/1000)/10;
            if (count >= 100000) {
                rounded = ceilf(count/10000)/100;
                return [NSString stringWithFormat:@"%.1fkk", rounded];
            }
            return [NSString stringWithFormat:@"%luk", (unsigned long)rounded];
        }
    }
    
    return [NSString stringWithFormat:@"%lu", (unsigned long)count];
}



@end
