//
//  FavouriteViewController.m
//  realestateapp
//
//  Created by Юрий Дурнев on 23/03/15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "FavouriteViewController.h"
#import "ListCell.h"
#import "MAdvert.h"
#import "DetailAdvert.h"

static NSString * const cellIdentifier = @"ListCell";

@interface FavouriteViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray *advertList;
@end

@implementation FavouriteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"Избранное";
    
    [[APIClient sharedClient] getFavoriteAdvertWithSuccessBlock:^(NSURLSessionDataTask *task, id responseObject) {
        self.advertList = [MAdvert deserializeAppInfosFromJSON:responseObject[@"advertisements"]];
        if (self.advertList.count > 0) {
             [self.tableView reloadData];
        }
    } andFailBlock:^(NSURLSessionDataTask *task, NSError *error) {
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    DetailAdvert *detailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"detailAdvert"];
    detailVC.adversementID = [_advertList[indexPath.row] adversementID];
    [self.navigationController pushViewController:detailVC animated:YES];
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ListCell *cell = (ListCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    MAdvert *advert = _advertList[indexPath.row];
    if (advert) {
        cell.advertTitle.text = advert.mapStreet == nil ? @"Не указано": advert.mapStreet;
        cell.advertPrice.text = [self priceFormatter:[NSNumber numberWithInteger:advert.price]];
        cell.advertPlace.text = advert.mapStreet == nil ? @"Не указано": advert.mapStreet;
        cell.advertDescription.text = advert.title == nil ? @"Не указано":advert.title;
        
        if (![advert.isPremium isEqualToString:@""]) {
            if (advert.isPremium) {
                cell.advertPriceBackground.backgroundColor = [UIColor colorWithRed:0.735 green:0.738 blue:0.004 alpha:1.000];
            }
        }
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            if (advert.images.count != 0) {
                NSString *imageURL = advert.images[0][@"mobile"];
                __weak UIImageView *weakSelf = cell.advertImage;
                [cell.advertImage sd_setImageWithURL:[NSURL URLWithString:imageURL]
                                    placeholderImage:[UIImage imageNamed:@"placeholder"]
                                             options:SDWebImageRetryFailed
                                           completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                               weakSelf.alpha = 0.0;
                                               [UIView animateWithDuration:0.35f
                                                                animations:^{
                                                                    weakSelf.alpha = 1.0;
                                                                }];
                                               
                                           }];
            }
            
        });
        
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _advertList.count;
}

- (NSString *)priceFormatter:(NSNumber *)price {
    NSNumberFormatter *frmtr = [[NSNumberFormatter alloc] init];
    [frmtr setGroupingSize:3];
    [frmtr setGroupingSeparator:@" "];
    [frmtr setUsesGroupingSeparator:YES];
    NSString *commaString = [frmtr stringFromNumber:price];
    if ([commaString isEqualToString:@"0"]) {
        commaString = @"";
        return commaString;
    } else {
        NSString *currency = @"₽";
        NSString *pruceWithCurrency = [NSString stringWithFormat:@"%@ %@", commaString, currency];
        return pruceWithCurrency;
    }
}

@end