//  XLFormWeekDaysCell.m
//  XLForm ( https://github.com/xmartlabs/XLForm )
//
//  Copyright (c) 2015 Xmartlabs ( http://xmartlabs.com )
//
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "XLFormRoomCell.h"

NSString * const XLFormRowDescriptorTypeWeekDays = @"XLFormRoom";

NSString *const kOneRoom = @"1";
NSString *const kTwoRoom = @"2";
NSString *const kThreeRoom = @"3";
NSString *const kFourRoom = @"4";
NSString *const kFivePlusRooms = @"5";

@interface XLFormRoomCell ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *oneButton;
@property (weak, nonatomic) IBOutlet UIButton *twoButton;
@property (weak, nonatomic) IBOutlet UIButton *threeButton;
@property (weak, nonatomic) IBOutlet UIButton *fourButton;
@property (weak, nonatomic) IBOutlet UIButton *fiveButton;
@property (strong, nonatomic) NSArray *buttonArray;
@end

@implementation XLFormRoomCell

+(void)load
{
    [XLFormViewController.cellClassesForRowDescriptorTypes setObject:NSStringFromClass([XLFormRoomCell class]) forKey:XLFormRowDescriptorTypeWeekDays];
}

#pragma mark - XLFormDescriptorCell

- (void)configure
{
    [super configure];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.buttonArray = @[self.oneButton, self.twoButton, self.threeButton, self.fourButton, self.fiveButton];
}

-(void)update
{
    [super update];
}

#pragma mark - Action

- (IBAction)dayTapped:(id)sender {
    [self dayTapped:sender day:[self getDayFormButton:sender]];
}

#pragma mark - Helpers

- (NSString *)getDayFormButton:(id)sender {
    if (sender == self.oneButton) return kOneRoom;
    if (sender == self.twoButton) return kTwoRoom;
    if (sender == self.threeButton) return kThreeRoom;
    if (sender == self.fourButton) return kFourRoom;
    if (sender == self.fiveButton) return kFivePlusRooms;
    return kOneRoom;
}

- (void)dayTapped:(UIButton *)button day:(NSString *)day {
    [self deselectAllButton];
    
    UIColor *color = [[UIColor alloc] initWithCGColor:button.layer.borderColor];
    if (!button.selected) {
        button.selected = YES;
        [button setBackgroundColor:color];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    } else  {
        [button setBackgroundColor:[UIColor whiteColor]];
        [button setTitleColor:color forState:UIControlStateNormal];
    }
  
    self.rowDescriptor.value = day;
}

- (void)deselectAllButton {
    for (UIButton *button in self.buttonArray) {
        button.selected = NO;
        UIColor *color = [[UIColor alloc] initWithCGColor:button.layer.borderColor];
        [button setBackgroundColor:[UIColor whiteColor]];
        [button setTitleColor:color forState:UIControlStateNormal];
    }
}

+ (CGFloat)formDescriptorCellHeightForRowDescriptor:(XLFormRowDescriptor *)rowDescriptor {
    return 98;
}


@end
