//
//  ColorNameCell.m
//  CollectionViewItemAnimations
//
//  Created by Nick Donaldson on 8/27/13.
//  Copyright (c) 2013 nd. All rights reserved.
//

#import "ColorNameCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation ColorNameCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.imageViewAdvert.layer.cornerRadius = 3.0f;
    self.imageViewAdvert.layer.masksToBounds = YES;
    self.imageViewAdvert.contentMode = UIViewContentModeScaleToFill;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
}



@end
