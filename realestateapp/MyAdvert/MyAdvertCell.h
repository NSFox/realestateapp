//
//  MyAdvertCell.h
//  realestateapp
//
//  Created by NSFox on 13.05.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyAdvertCell : MGSwipeTableCell
@property (weak, nonatomic) IBOutlet UIImageView *advertImage;
@property (weak, nonatomic) IBOutlet UILabel *advertPrice;
@property (weak, nonatomic) IBOutlet UILabel *advertTitle;
@property (weak, nonatomic) IBOutlet UILabel *advertDescription;
@property (weak, nonatomic) IBOutlet UILabel *advertPlace;
@end
