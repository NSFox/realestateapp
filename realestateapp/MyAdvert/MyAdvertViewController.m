//
//  MyAdvertViewController.m
//  realestateapp
//
//  Created by NSFox on 13.05.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "MyAdvertViewController.h"
#import "LoginViewController.h"
#import "LoginModel.h"
#import "RLProfilViewController.h"
#import "MAdvert.h"
#import "MyAdvertCell.h"
#import "DetailAdvert.h"

static NSString * const cellIdentifier = @"ListCell";

@interface MyAdvertViewController () <UITableViewDelegate, UITableViewDataSource, MGSwipeTableCellDelegate>
@property (weak, nonatomic) IBOutlet UILabel *UserEmailLabel;
@property (strong, nonatomic) LoginModel *user;
@property (nonatomic, strong) NSArray *advertList;
@property (nonatomic, strong) NSMutableArray *active;
@property (nonatomic, strong) NSMutableArray *deactive;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation MyAdvertViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.active = [NSMutableArray new];
    self.deactive = [NSMutableArray new];
    [self loginUser];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [TSMessage dismissActiveNotification];
}

- (void)loginUser {
    BOOL isLogin = [[NSUserDefaults standardUserDefaults] objectForKey:@"isLogin"];
    if (!isLogin) {
        LoginViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"loginViewController"];
        [self presentViewController:vc animated:YES completion:nil];
    } else {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSData *encodedObject = [defaults objectForKey:@"userLogin"];
        self.user = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
        self.UserEmailLabel.text = self.user.userMail;
        [self loadData];
    }
}

- (void)loadData {
    [[APIClient sharedClient] getMyAdvertWithSuccessBlock:^(NSURLSessionDataTask *task, id responseObject)  {
        
        self.advertList = [MAdvert deserializeAppInfosFromJSON:responseObject[@"advertisements"]];
        if (self.advertList.count > 0) {
            for (MAdvert *item in self.advertList) {
                if (item.status.integerValue == 1) {
                    [self.active addObject:item];
                } else if (item.status.integerValue == 2) {
                    [self.deactive addObject:item];
                }
            }
            [self.tableView reloadData];
        }
    } andFailBlock:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)logOut:(UIBarButtonItem *)sender {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userLogin"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"isLogin"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self loginUser];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    DetailAdvert *detailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"detailAdvert"];
    detailVC.adversementID = [_advertList[indexPath.row] adversementID];
    [self.navigationController pushViewController:detailVC animated:YES];
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MyAdvertCell *cell = (MyAdvertCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.delegate = self;
    cell.rightSwipeSettings.transition = MGSwipeTransitionStatic;
    cell.rightExpansion.fillOnTrigger = NO;
    cell.rightButtons = [self createRightButtons:2 section:indexPath.section];
    
    MAdvert *advert = _advertList[indexPath.row];
    
    if (advert) {
        cell.advertTitle.text = advert.mapStreet == nil ? @"Не указано": advert.mapStreet;
        cell.advertPrice.text = [self priceFormatter:[NSNumber numberWithInteger:advert.price]];
        cell.advertPlace.text = advert.mapStreet == nil ? @"Не указано": advert.mapStreet;
        cell.advertDescription.text = advert.title == nil ? @"Не указано":advert.title;
        
//        if (![advert.isPremium isEqualToString:@""]) {
//            if (advert.isPremium) {
//                cell.advertPriceBackground.backgroundColor = [UIColor colorWithRed:0.735 green:0.738 blue:0.004 alpha:1.000];
//            }
//        }
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            if (advert.images.count != 0) {
                NSString *imageURL = advert.images[0][@"mobile"];
                __weak UIImageView *weakSelf = cell.advertImage;
                [cell.advertImage sd_setImageWithURL:[NSURL URLWithString:imageURL]
                                    placeholderImage:[UIImage imageNamed:@"placeholder"]
                                             options:SDWebImageRetryFailed
                                           completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                               weakSelf.alpha = 0.0;
                                               [UIView animateWithDuration:0.35f
                                                                animations:^{
                                                                    weakSelf.alpha = 1.0;
                                                                }];
                                               
                                           }];
            }
            
        });
        
    }
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0: return [NSString stringWithFormat:@"АКТИВНЫЕ ОБЪЯВЛЕНИЯ (%lu)", (unsigned long) self.active.count];
            break;
        case 1: return [NSString stringWithFormat:@"НЕАКТИВНЫЕ ОБЪЯВЛЕНИЯ (%lu)", (unsigned long) self.deactive.count];
            break;
        default:
            break;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    view.tintColor = [UIColor whiteColor];
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:[UIColor grayColor]];
    header.textLabel.font = [UIFont fontWithName:@"AvenirNext-Medium" size:14];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0: return self.active.count;
            break;
        case 1: return self.deactive.count;
            break;
        default:
            break;
    }
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (BOOL)swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion {
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    MAdvert *advert = _advertList[indexPath.row];
  
    switch (indexPath.section) {
        case 0: if (index == 1) {
                    //edit advert
                } else {
                    [self deactiveAdvertOnAdversementID:advert.adversementID];
                }
            break;
        case 1: if (index == 1) {
                    //edit advert
                } else {
                    [self deleteAdvertOnAdversementID:advert.adversementID];
                }
            break;
        default:
            break;
    }
    return YES;
}

- (void)deleteAdvertOnAdversementID:(NSString *)adversementID {
    [[APIClient sharedClient] deleteAdvert:adversementID withSuccessBlock:^(NSURLSessionDataTask *task, id responseObject) {
        [[RLAlertNotificationView sharedClient] addedMessageTitle:@"Внимание" message:@"Объявление удалено" onView:self.navigationController];
        [self loadData];
    } andFailBlock:nil];
}

- (void)deactiveAdvertOnAdversementID:(NSString *)adversementID {
    [[APIClient sharedClient] deactiveAdvert:adversementID withSuccessBlock:^(NSURLSessionDataTask *task, id responseObject) {
        [[RLAlertNotificationView sharedClient] addedMessageTitle:@"Внимание" message:@"Объявление деактивировано" onView:self.navigationController];
        [self loadData];
    } andFailBlock:nil];
}

- (NSArray *)createRightButtons:(NSUInteger )number section:(NSUInteger )section {
    NSMutableArray * result = [NSMutableArray array];
    UIColor *colors[2] = {[UIColor colorWithRed:0.820 green:0.302 blue:0.322 alpha:1.000], [UIColor colorWithRed:0.596 green:0.298 blue:0.557 alpha:1.000]};
    NSArray *icons;
    
    switch (section) {
        case 0: icons = @[[UIImage imageNamed:@"DeactiveIcon"], [UIImage imageNamed:@"EditIcon"]];
            break;
        case 1: icons = @[[UIImage imageNamed:@"DeleteIcon"], [UIImage imageNamed:@"EditIcon"]];
            break;
        default:
            break;
    }
    
    for (int i = 0; i < number; ++i) {
        MGSwipeButton * button = [MGSwipeButton buttonWithTitle:@"" icon:icons[i] backgroundColor:colors[i] padding:15 callback:nil];
        [result addObject:button];
    }
    return result;
}

- (NSString *)priceFormatter:(NSNumber *)price {
    NSNumberFormatter *frmtr = [[NSNumberFormatter alloc] init];
    [frmtr setGroupingSize:3];
    [frmtr setGroupingSeparator:@" "];
    [frmtr setUsesGroupingSeparator:YES];
    NSString *commaString = [frmtr stringFromNumber:price];
    if ([commaString isEqualToString:@"0"]) {
        commaString = @"";
        return commaString;
    } else {
        NSString *currency = @"₽";
        NSString *pruceWithCurrency = [NSString stringWithFormat:@"%@ %@", commaString, currency];
        return pruceWithCurrency;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString: @"profil"]) {
        RLProfilViewController *vc = segue.destinationViewController;
        vc.user = self.user;
    }
}

@end
