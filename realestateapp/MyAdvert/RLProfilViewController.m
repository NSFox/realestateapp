//
//  RLProfilViewController.m
//  realestateapp
//
//  Created by Юрий Дурнев on 27.07.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "RLProfilViewController.h"

@interface RLProfilViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *userLastNameText;
@property (weak, nonatomic) IBOutlet UITextField *userFirstNameText;
@property (weak, nonatomic) IBOutlet UITextField *userEmailText;
@property (weak, nonatomic) IBOutlet UITextField *userPhoneText;
@property (weak, nonatomic) IBOutlet UITextField *userSkypeText;

@end

@implementation RLProfilViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.userLastNameText.delegate = self;
    self.userFirstNameText.delegate = self;
    self.userEmailText.delegate = self;
    self.userPhoneText.delegate = self;
    self.userSkypeText.delegate = self;
    
    [self textForTextField:self.user];
}

- (void)textForTextField:(LoginModel *)user {
    self.userLastNameText.text = user.userLastName;
    self.userFirstNameText.text = user.userFirstName;
    self.userEmailText.text = user.userMail;
    self.userPhoneText.text = user.userPhone;
    self.userSkypeText.text = user.userSkype;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)saveProfil:(UIBarButtonItem *)sender {
    
    NSMutableDictionary *userInfo = [NSMutableDictionary new];
    
    if (self.userLastNameText.text.length > 0) {
        userInfo[@"last_name"] = self.userLastNameText.text;
    } else {
        [self alertWithMessage:@"Заполните поле \"Фамилия\""];
    }
    
    if (self.userFirstNameText.text.length > 0) {
        userInfo[@"first_name"] = self.userFirstNameText.text;
    } else {
        [self alertWithMessage:@"Заполните поле \"Имя\""];
    }

    if (self.userPhoneText.text.length > 0) {
        userInfo[@"mobile"] = self.userPhoneText.text;
    } else {
        [self alertWithMessage:@"Заполните поле \"Телефон\""];
    }
    
    if (self.userSkypeText.text.length > 0) {
        userInfo[@"skype"] = self.userSkypeText.text;
    } else {
        [self alertWithMessage:@"Заполните поле \"Skype\""];
    }
    
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    if (token.length > 0 && userInfo.count > 0) {
        NSDictionary *params = @{@"auth_token": token,
                                 @"user_info": userInfo};
        
        [[APIClient sharedClient] saveNewDataUserWithParams:params withSuccessBlock:^(NSURLSessionDataTask *task, id responseObject) {
            [self errorServer:responseObject];
        } andFailBlock:^(NSURLSessionDataTask *task, NSError *error) {
            [self alertWithMessage:error.description];
        }];
    }
}

- (void)errorServer:(id)responseObject {
    if (responseObject[@"error"] != [NSNull null] ) {
        NSDictionary *errorArray = responseObject[@"error"];
        NSInteger errorCode = [errorArray[@"code"] integerValue];
        NSLog(@"%li Description: %@", (long)errorCode, errorArray[@"description"]);
        [self alertWithMessage:errorArray[@"description"]];
    } else {
        NSDictionary *userInfo = responseObject[@"user_info"];
        NSLog(@"%@", responseObject);
        LoginModel *user = [LoginModel deserializeAppInfosFromJSON:userInfo];
        user.userPass = self.user.userPass;
        if (user) {
            NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:user];
            [[NSUserDefaults standardUserDefaults] setObject:encodedObject forKey:@"userLogin"];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isLogin"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        [self textForTextField:user];
        
        [self alertWithMessage:@"Данные успешно изменены"];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)alertWithMessage:(NSString *)message {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Внимание" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil,nil];
    [alert show];
}

@end
