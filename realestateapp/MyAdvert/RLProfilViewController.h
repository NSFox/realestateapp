//
//  RLProfilViewController.h
//  realestateapp
//
//  Created by Юрий Дурнев on 27.07.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginModel.h"

@interface RLProfilViewController : UITableViewController

@property (nonatomic, strong) LoginModel *user;

@end
