//
//  LeftMenuViewController.m
//  opinion
//
//  Created by Юрий Дурнев on 16/03/15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "LeftMenuViewController.h"

#define cellHeight 60.0f

@interface LeftMenuViewController () <UITableViewDataSource, UITableViewDelegate>
@property (strong, readwrite, nonatomic) UITableView *tableView;
@property (nonatomic, strong) NSArray *titles;
@property (nonatomic, strong) NSArray *images;
@property (nonatomic, strong) NSArray *imagesSelected;
@end

@implementation LeftMenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView = ({
        UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, (self.view.frame.size.height - cellHeight * 7) / 2.0f, self.view.frame.size.width, cellHeight * 7) style:UITableViewStylePlain];
        tableView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth;
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.opaque = NO;
        tableView.backgroundColor = [UIColor clearColor];
        tableView.backgroundView = nil;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        tableView.bounces = NO;
        tableView.scrollsToTop = NO;
        tableView;
    });
    [self.view addSubview:self.tableView];
    
    _titles = @[@"Поиск", @"Избранное", @"Новое объявление", @"Личный кабинет", @"О приложении", @"Помощь"];
    _images = @[@"search", @"star", @"plus", @"user", @"info", @"help"];
    _imagesSelected = @[@"searchSelected", @"starSelected", @"plusSelected", @"userSelected", @"infoSelected", @"helpSelected"];
}

- (void)addViewController:(NSString *)identifier {
    UINavigationController *rootViewController = [[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:identifier]];
    [self.sideMenuViewController setContentViewController:rootViewController animated:YES];
    [self.sideMenuViewController hideMenuViewController];
}

#pragma mark - UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView reloadData];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.textLabel.text = _titles[indexPath.row];
    cell.textLabel.font = [UIFont fontWithName:@"AvenirNext-Medium" size:18];
    cell.textLabel.textColor = [UIColor colorWithRed:0.959 green:0.295 blue:0.317 alpha:1.000];
    cell.imageView.image = [UIImage imageNamed:_imagesSelected[indexPath.row]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    switch (indexPath.row) {
        case 0: [self addViewController:@"firstViewController"];
            break;
        case 1: [self addViewController:@"favoriteViewController"];
            break;
        case 2: [self addViewController:@"newAdvertViewController"];
            break;
        case 3: [self addViewController:@"myAdvertViewController"];
            break;
        case 5: [self addViewController:@"supportViewController"];
            break;
        default:
            break;
    }
}

#pragma mark -
#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return cellHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex {
    return _titles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.font = [UIFont fontWithName:@"AvenirNext-Medium" size:18];
    cell.textLabel.textColor = [UIColor blackColor];
    cell.textLabel.text = _titles[indexPath.row];
    cell.imageView.image = [UIImage imageNamed:_images[indexPath.row]];
    
    return cell;
}

@end
