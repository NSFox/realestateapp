//
//  RLRegionModel.m
//  realestateapp
//
//  Created by Юрий Дурнев on 29.07.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "RLRegionModel.h"

@implementation RLRegionModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"fullName": @"full_name",
             @"region": @"region"};
}

+ (NSArray *)deserializingRegion:(NSArray *)region {
    NSError *error;
    
    NSArray *dictModel = [MTLJSONAdapter modelsOfClass:[RLRegionModel class] fromJSONArray:region error:&error];
    if (error) {
        NSLog(@"Couldn't convert app infos JSON to %@ models: %@", [RLRegionModel class], error);
        return nil;
    }
    
    return dictModel;
}

+ (RLRegionModel *)deserializingRegionLocation:(NSDictionary *)region {
    NSError *error;
    
    RLRegionModel *dictModel = [MTLJSONAdapter modelOfClass:[RLRegionModel class] fromJSONDictionary:region error:&error];
    if (error) {
        NSLog(@"Couldn't convert app infos JSON to %@ models: %@", [RLRegionModel class], error);
        return nil;
    }
    
    return dictModel;
}

@end
