//
//  RLCategoryModel.h
//  realestateapp
//
//  Created by Юрий Дурнев on 29.07.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "MTLModel.h"

@interface RLCategoryModel : MTLModel <MTLJSONSerializing>
@property (nonatomic, copy) NSString *categoryName;
@property (nonatomic, copy) NSString *category;
@property (nonatomic, copy) NSString *advertType;
@property (readonly, nonatomic, assign, getter = isLeaf) BOOL Leaf;

+ (NSArray *)deserizarilationCategory:(NSArray *)array;

@end
