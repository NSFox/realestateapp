//
//  RLCategoryModel.m
//  realestateapp
//
//  Created by Юрий Дурнев on 29.07.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "RLCategoryModel.h"

@implementation RLCategoryModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"categoryName": @"category_name",
             @"category": @"category",
             @"Leaf": @"is_leaf",
             @"advertType":@"advert_type"};
}

+ (NSArray *)deserizarilationCategory:(NSArray *)array {
    NSError *error;
    
    NSArray *dictModel = [MTLJSONAdapter modelsOfClass:[RLCategoryModel class] fromJSONArray:array error:&error];
    if (error) {
        NSLog(@"Couldn't convert app infos JSON to DetailAdvertModel models: %@", error);
        return nil;
    }
    
    return dictModel;
}

@end
