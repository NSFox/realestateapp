//
//  RLGroupFieldsModel.h
//  realestateapp
//
//  Created by Юрий Дурнев on 30.07.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "MTLModel.h"

@interface RLCustomFields : MTLModel <MTLJSONSerializing>
@property (nonatomic, copy) NSString *dictionaryField;
@property (nonatomic, copy) NSString *dependentField;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSArray *field_values;
@property (readonly, nonatomic, assign, getter = isRequired) BOOL required;

+ (NSArray *)deserializationCustomFields:(NSArray *)fields;
@end

@interface RLGroupFieldsModel : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *sortIndex;
@property (nonatomic, copy) NSArray *customFields;

+ (NSArray *)deserializationGroupFields:(NSArray *)fields;
@end
