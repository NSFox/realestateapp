//
//  RLRegionModel.h
//  realestateapp
//
//  Created by Юрий Дурнев on 29.07.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "MTLModel.h"

@interface RLRegionModel : MTLModel <MTLJSONSerializing>
@property (nonatomic, copy) NSString *fullName;
@property (nonatomic, copy) NSString *region;

+ (NSArray *)deserializingRegion:(NSArray *)region;
+ (RLRegionModel *)deserializingRegionLocation:(NSDictionary *)region;

//http://irr.ru/mobile_api/1.2/regions/cities?region=russia/voronezhskaya-obl/

@end
