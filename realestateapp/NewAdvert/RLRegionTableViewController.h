//
//  RLRegionTableViewController.h
//  realestateapp
//
//  Created by Юрий Дурнев on 29.07.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RLRegionTableViewController : UITableViewController<XLFormRowDescriptorViewController>
@property (strong, nonatomic) NSMutableDictionary *params;
@property (nonatomic) BOOL leafCategory;

@end
