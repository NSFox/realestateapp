//
//  RLSelectionNavigationViewController.m
//  realestateapp
//
//  Created by Юрий Дурнев on 29.07.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "RLSelectionNavigationViewController.h"
#import "RLCategoryModel.h"
#import "XLFormRightDetailCell.h"

#define CELL_REUSE_IDENTIFIER  @"OptionCell"

@interface RLSelectionNavigationViewController ()
@property (strong, nonatomic) NSMutableArray *category;
@end

@implementation RLSelectionNavigationViewController
@synthesize rowDescriptor = _rowDescriptor;


- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:CELL_REUSE_IDENTIFIER];
    
    if (!self.params) {
        self.params = [NSMutableDictionary new];
        self.params.dictionary = @{@"category":@"real-estate"};
    }
    
    [[APIClient sharedClient] getCategoryWithParams:self.params withSuccessBlock:^(NSURLSessionDataTask *task, id responseObject) {
        if (!self.category) {
            self.category = [NSMutableArray new];
        }
        self.category.array = [RLCategoryModel deserizarilationCategory:responseObject[@"categories"]];
        [self.category removeObjectAtIndex:0];
        [self.tableView reloadData];
    } andFailBlock:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self category] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CELL_REUSE_IDENTIFIER forIndexPath:indexPath];
    RLCategoryModel *model = self.category[indexPath.row];
    cell.textLabel.text = model.categoryName;
    if (!model.isLeaf) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    RLCategoryModel *model = self.category[indexPath.row];
    if (model.isLeaf) {
        if (model.advertType.length > 0) {
            self.rowDescriptor.selectorOptions = @[model.category, model.advertType];
        }
        
        self.rowDescriptor.value = model.categoryName;
        [self.navigationController popToRootViewControllerAnimated:YES];
    } else {
        RLSelectionNavigationViewController *vc = [[RLSelectionNavigationViewController alloc] init];
        vc.rowDescriptor = self.rowDescriptor;
        vc.title = model.categoryName;
        vc.params = [NSMutableDictionary new];
        vc.params.dictionary = @{@"category":model.category};
        [self.navigationController pushViewController:vc animated:YES];
    }
}

@end
