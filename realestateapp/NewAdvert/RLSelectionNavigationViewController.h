//
//  RLSelectionNavigationViewController.h
//  realestateapp
//
//  Created by Юрий Дурнев on 29.07.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "XLFormOptionsViewController.h"
#import <XLFormRowDescriptor.h>

@interface RLSelectionNavigationViewController : UITableViewController <XLFormRowDescriptorViewController>
@property (strong, nonatomic) NSMutableDictionary *params;

@end
