//
//  RLRegionTableViewController.m
//  realestateapp
//
//  Created by Юрий Дурнев on 29.07.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "RLRegionTableViewController.h"
#import "RLRegionModel.h"

NSString * const cellIdentifier = @"RegionCell";

@interface RLRegionTableViewController ()
@property (strong, nonatomic) NSArray *region;
@end

@implementation RLRegionTableViewController
@synthesize rowDescriptor = _rowDescriptor;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;

    if (!self.leafCategory) {
        [[APIClient sharedClient] getRegionWithSuccessBlock:^(NSURLSessionDataTask *task, id responseObject) {
            self.region = [RLRegionModel deserializingRegion:responseObject[@"regions"]];
            [self.tableView reloadData];
        } andFailBlock:nil];
    } else {
        [[APIClient sharedClient] getCityWithParams:self.params withSuccessBlock:^(NSURLSessionDataTask *task, id responseObject) {
            self.region = [RLRegionModel deserializingRegion:responseObject[@"regions"]];
            [self.tableView reloadData];
        } andFailBlock:nil];
    }
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:cellIdentifier];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.region.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    RLRegionModel *model = self.region[indexPath.row];
    cell.textLabel.text = model.fullName;
    
    if (!self.leafCategory && ![model.fullName isEqualToString:@"Вся Россия"])  {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    RLRegionModel *model = self.region[indexPath.row];
    if (self.leafCategory || [model.fullName isEqualToString:@"Вся Россия"]) {
        self.rowDescriptor.selectorOptions = @[model.region];
        self.rowDescriptor.value = model.fullName;
        [self.navigationController popToRootViewControllerAnimated:YES];
    } else {
        RLRegionTableViewController *vc = [[RLRegionTableViewController alloc] init];
        vc.rowDescriptor = self.rowDescriptor;
        vc.title = model.fullName;
        vc.params = [NSMutableDictionary new];
        vc.params.dictionary = @{@"region":model.region};
        vc.leafCategory = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

@end
