//
//  RLNewAdvertViewController.m
//  realestateapp
//
//  Created by Юрий Дурнев on 22.07.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "RLNewAdvertViewController.h"
#import "XLPhotoChangeCell.h"
#import "XLContactCell.h"
#import "XLTextAdvert.h"
#import "RLSelectionNavigationViewController.h"
#import "RLRegionTableViewController.h"
#import "RLRegionModel.h"
#import "LoginModel.h"
#import "RLGroupFieldsModel.h"

NSString * const kSelectorCategory = @"kSelectorCategory";
NSString * const kSelectorCity = @"kSelectorCity";
NSString * const kSelector = @"kSelector";
NSString * const kPhotoChange = @"kPhotoChange";
NSString * const kContactCell = @"kContactCell";
NSString * const kAdvertText = @"kAdvertText";
NSString * const khiderow = @"tag1";

@interface RLNewAdvertViewController () <XLPhotoChangeDelegate>
@property (strong, nonatomic) NSMutableDictionary *dictCategoryRegion;
@property (strong, nonatomic) NSMutableArray *requaerdFields;
@property (strong, nonatomic) NSMutableArray *imageArray;
@property (strong, nonatomic) LoginModel *user;
@end

@implementation RLNewAdvertViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (!self.dictCategoryRegion) {
        self.dictCategoryRegion = [NSMutableDictionary new];
    }
    [self initializeForm];
    self.title = @"Новое объявление";
}

- (void)savePressed {
    NSDictionary *dict = [self resultValue];
    if (dict != nil) {
        [self.dictCategoryRegion setObject:dict forKey:@"advertisement"];
        NSLog(@"%@", self.dictCategoryRegion);
        [[APIClient sharedClient] postAdvertWithParams:self.dictCategoryRegion
                                                 image:self.imageArray
                                      withSuccessBlock:^(NSURLSessionDataTask *task, id responseObject) {
                                          if (responseObject[@"error"] != [NSNull null] ) {
                                              NSDictionary *errorArray = responseObject[@"error"];
                                              NSInteger errorCode = [errorArray[@"code"] integerValue];
                                              NSNumber *statusCode = @((long) errorCode);
                                              if (statusCode.integerValue) {
                                                  [self alertWithMessages:errorArray[@"description"] andTitle:@"Ошибка"];
                                              }
                                          } else {
                                              [self alertWithMessages:@"Объявление успешно подано!" andTitle:@"Поздравляем"];
                                          }
                                      } andFailBlock:^(NSURLSessionDataTask *task, NSError *error) {
                                          NSLog(@"%@", error);
                                      }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initializeForm {
    XLFormDescriptor *form = [XLFormDescriptor formDescriptor];
    XLFormRowDescriptor *row;
    
    //------------------------------------------------------------------------------------------------------------------
    XLFormSectionDescriptor *section = [XLFormSectionDescriptor formSection];
    [form addFormSection:section];

    XLFormRowDescriptor *rowCategory = [XLFormRowDescriptor formRowDescriptorWithTag:@"category" rowType:XLFormRowDescriptorTypeSelectorPush];
    [self rowConfig:rowCategory withTitle:@"КАТЕГОРИЯ*" andValuePlaceholder:@"Не выбрано" withTypeRow:@"dictionary"];
    rowCategory.action.viewControllerClass = [RLSelectionNavigationViewController class];
    [section addFormRow:rowCategory];

    XLFormRowDescriptor *rowCity = [XLFormRowDescriptor formRowDescriptorWithTag:@"region" rowType:XLFormRowDescriptorTypeSelectorPush];
    
    [[APIClient sharedClient] getLocationWithSuccessBlock:^(NSURLSessionDataTask *task, id responseObject) {
        RLRegionModel *model = [RLRegionModel deserializingRegionLocation:responseObject[@"values"]];
        if (model != nil) {
            [self rowConfig:rowCity withTitle:@"ГОРОД*" andValuePlaceholder:model.fullName withTypeRow:@"dictionary"];
            self.dictCategoryRegion[@"region"] = model.region;
        }
    } andFailBlock:^(NSURLSessionDataTask *task, NSError *error) {
        [self rowConfig:rowCity withTitle:@"ГОРОД*" andValuePlaceholder:@"Не выбрано" withTypeRow:@"dictionary"];
    }];
    
    rowCity.hidden = [NSString stringWithFormat:@"$%@.value == 'Не выбрано'", rowCategory];
    rowCity.action.viewControllerClass = [RLRegionTableViewController class];
    [section addFormRow:rowCity];
    
    //------------------------------------------------------------------------------------------------------------------
    XLFormSectionDescriptor *sectionPhoto;
    sectionPhoto = [XLFormSectionDescriptor formSection];
    sectionPhoto.hidden = [NSString stringWithFormat:@"($%@.value == 'Не выбрано') || ($%@.value == 'Не выбрано')", rowCity, rowCategory];
    [form addFormSection:sectionPhoto];
    
    row = [XLFormRowDescriptor formRowDescriptorWithTag:kPhotoChange rowType:XLPhotoChangeCollectionView];
    [row.cellConfig setObject:self forKey:@"viewContoller"];
    [sectionPhoto addFormRow:row];
    
    //------------------------------------------------------------------------------------------------------------------
    XLFormSectionDescriptor *sectionContact;
    sectionContact = [XLFormSectionDescriptor formSection];
    sectionContact.hidden = [NSString stringWithFormat:@"($%@.value == 'Не выбрано') || ($%@.value == 'Не выбрано')", rowCity, rowCategory];
    [form addFormSection:sectionContact];
    
    [self addContactRow:row section:sectionContact];
    
    XLFormSectionDescriptor *sectionSend;
    sectionSend = [XLFormSectionDescriptor formSection];
    sectionSend.hidden = [NSString stringWithFormat:@"($%@.value == 'Не выбрано') || ($%@.value == 'Не выбрано')", rowCity, rowCategory];
    
    XLFormRowDescriptor * buttonLeftAlignedRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"send" rowType:XLFormRowDescriptorTypeButton title:@"Опубликовать"];
    [buttonLeftAlignedRow.cellConfigAtConfigure setObject:[UIColor colorWithRed:0.961 green:0.294 blue:0.318 alpha:1.000] forKey:@"backgroundColor"];
    [buttonLeftAlignedRow.cellConfig setObject:[UIColor whiteColor] forKey:@"textLabel.textColor"];
    [buttonLeftAlignedRow.cellConfig setObject:@(NSTextAlignmentCenter) forKey:@"textLabel.textAlignment"];
    [buttonLeftAlignedRow.cellConfig setObject:[UIFont fontWithName:@"AvenirNext-Medium" size:14.0] forKey:@"detailTextLabel.font"];
    
    __typeof(self) __weak weakSelf = self;
    buttonLeftAlignedRow.action.formBlock = ^(XLFormRowDescriptor *sender){
        [weakSelf savePressed];
        [weakSelf deselectFormRow:sender];
    };
    
    [sectionSend addFormRow:buttonLeftAlignedRow];
    
    [form addFormSection:sectionSend];
    
    self.form = form;
}

- (void)addContactRow:(XLFormRowDescriptor *)row section:(XLFormSectionDescriptor *)section {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [defaults objectForKey:@"userLogin"];
    self.user = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    
    if (self.user) {
        row = [XLFormRowDescriptor formRowDescriptorWithTag:@"email" rowType:XLContactText];
        [row.cellConfig setObject:@"ЭЛЕКТРОННАЯ ПОЧТА/ЛОГИН" forKey:@"titleLabel.text"];
        [row.cellConfig setObject:self.user.userMail forKey:@"textFieldContact.text"];
        row.disabled = @YES;
        [row.cellConfig setObject:@NO forKey:@"textFieldContact.enabled"];
        [section addFormRow:row];
        
        row = [XLFormRowDescriptor formRowDescriptorWithTag:@"seller" rowType:XLContactText];
        [row.cellConfig setObject:@"КОНТАКТНОЕ ЛИЦО" forKey:@"titleLabel.text"];
        [row.cellConfig setObject:self.user.userFirstName forKey:@"textFieldContact.text"];
        row.disabled = @YES;
        [row.cellConfig setObject:@NO forKey:@"textFieldContact.enabled"];
        [section addFormRow:row];
        
        row = [XLFormRowDescriptor formRowDescriptorWithTag:@"phone" rowType:XLContactText];
        [row.cellConfig setObject:@"КОНТАКТНЫЙ ТЕЛЕФОН" forKey:@"titleLabel.text"];
        [row.cellConfig setObject:self.user.userPhone forKey:@"textFieldContact.text"];
        [row.cellConfig setObject:@NO forKey:@"textFieldContact.enabled"];
        row.disabled = @YES;
        [section addFormRow:row];
        
        [self.dictCategoryRegion setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forKey:@"auth_token"];
        
    } else {
        row = [XLFormRowDescriptor formRowDescriptorWithTag:@"email" rowType:XLContactText];
        [row.cellConfig setObject:@"ЭЛЕКТРОННАЯ ПОЧТА/ЛОГИН*" forKey:@"titleLabel.text"];
        row.required = YES;
        [row addValidator:[XLFormValidator emailValidator]];
        [section addFormRow:row];
        
        row = [XLFormRowDescriptor formRowDescriptorWithTag:@"password" rowType:XLContactText];
        [row.cellConfig setObject:@"ПАРОЛЬ*" forKey:@"titleLabel.text"];
        row.required = YES;
        [row addValidator:[XLFormRegexValidator formRegexValidatorWithMsg:@"Пароль должен быть больше 5, и меньше 32 символов" regex:@"^(?=).{5,32}$"]];
        [section addFormRow:row];
        
        row = [XLFormRowDescriptor formRowDescriptorWithTag:@"seller" rowType:XLContactText];
        [row.cellConfig setObject:@"КОНТАКТНОЕ ЛИЦО" forKey:@"titleLabel.text"];
        [section addFormRow:row];
        
        row = [XLFormRowDescriptor formRowDescriptorWithTag:@"phone" rowType:XLContactText];
        [row.cellConfig setObject:@"КОНТАКТНЫЙ ТЕЛЕФОН" forKey:@"titleLabel.text"];
        [section addFormRow:row];
    }
}

- (void)rowConfig:(XLFormRowDescriptor *)row withTitle:(NSString *)title andValuePlaceholder:(NSString *)placeholder withTypeRow:(NSString *)rowType {
    [row.cellConfig setObject:[UIFont fontWithName:@"AvenirNext-Medium" size:12.0] forKey:@"textLabel.font"];
    [row.cellConfig setObject:[UIColor grayColor] forKey:@"textLabel.textColor"];
    [row.cellConfig setObject:[UIFont fontWithName:@"AvenirNext-Medium" size:14.0] forKey:@"detailTextLabel.font"];
    [row.cellConfig setObject:[UIColor colorWithRed:0.961 green:0.294 blue:0.318 alpha:1.000] forKey:@"detailTextLabel.textColor"];
    row.title = title;
    
    if ([rowType isEqualToString:@"string"] || [rowType isEqualToString:@"integer"] || [rowType isEqualToString:@"float"]) {
        [row.cellConfig setObject:[UIFont fontWithName:@"AvenirNext-Medium" size:14.0] forKey:@"textField.font"];
        [row.cellConfig setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
        [row.cellConfig setObject:[UIColor colorWithRed:0.961 green:0.294 blue:0.318 alpha:1.000] forKey:@"textField.placeholderLabel.textColor"];
        [row.cellConfig setObject:@"не указано" forKey:@"textField.placeholder"];
        [row.cellConfig setObject:[UIColor colorWithRed:0.961 green:0.294 blue:0.318 alpha:1.000] forKey:@"textField.textColor"];
    } else {
        row.selectorTitle = title;
        row.value = placeholder;
    }
}

- (NSString *)typeRow:(NSString *)type {
    if ([type isEqualToString:@"string"]) {
        return XLFormRowDescriptorTypeText;
    }
    
    if ([type isEqualToString:@"integer"]) {
        return XLFormRowDescriptorTypeInteger;
    }
    
    if ([type isEqualToString:@"float"]) {
        return XLFormRowDescriptorTypeDecimal;
    }
    
    if ([type isEqualToString:@"bool"]) {
        return XLFormRowDescriptorTypeBooleanSwitch;
    }
    
    if ([type isEqualToString:@"dictionary"]) {
        return XLFormRowDescriptorTypeSelectorPush;
    }
    
    if ([type isEqualToString:@"text"]) {
        return XLTextAdvertCell;
    }
    
    return XLFormRowDescriptorTypeText;
}

- (void)formRowDescriptorValueHasChanged:(XLFormRowDescriptor *)rowDescriptor oldValue:(id)oldValue newValue:(id)newValue {
    [super formRowDescriptorValueHasChanged:rowDescriptor oldValue:oldValue newValue:newValue];
    
    if ([rowDescriptor.tag isEqualToString:@"category"]) {
        if (![[rowDescriptor.value valueData] isEqualToString:@"Не выбрано"]) {
            self.dictCategoryRegion[rowDescriptor.tag] = rowDescriptor.selectorOptions[0];
            //добавляем advert type
            self.dictCategoryRegion[@"advert_type"] = rowDescriptor.selectorOptions[1];
        }
    }
    if ([rowDescriptor.tag isEqualToString:@"region"]) {
        if (![[rowDescriptor.value valueData] isEqualToString:[rowDescriptor.value valueData]]) {
            self.dictCategoryRegion[rowDescriptor.tag] = rowDescriptor.selectorOptions[0];
        }
    }
    
    if ([rowDescriptor.tag isEqualToString:@"category"] || [rowDescriptor.tag isEqualToString:@"region"]) {
        [[APIClient sharedClient] getPostAdvertWithParams:self.dictCategoryRegion
                                         withSuccessBlock:^(NSURLSessionDataTask *task, id responseObject) {
                                             
                                             NSArray *groupArray = [RLGroupFieldsModel deserializationGroupFields:responseObject[@"group_custom_fields"]];
                                             if (groupArray.count != 0) {
                                                 [self addCustomField:groupArray];
                                             }
                                         } andFailBlock:^(NSURLSessionDataTask *task, NSError *error) {
                                             
                                         }];
    }
}

- (void)addCustomField:(NSArray *)field {
    XLFormSectionDescriptor *section = [XLFormSectionDescriptor formSection];
    if (field.count > 0) {
        RLGroupFieldsModel *customeField = field[0];
        for (RLCustomFields *custom in customeField.customFields) {
            XLFormRowDescriptor *row = [XLFormRowDescriptor formRowDescriptorWithTag:custom.name rowType:[self typeRow:custom.type]];
            NSString *title = custom.required ? [NSString stringWithFormat:@"%@*", [custom.title uppercaseString]] : [custom.title uppercaseString];
            [self rowConfig:row withTitle:title andValuePlaceholder:@"не выбрано" withTypeRow:custom.type];
            if (custom.field_values.count != 0) {
                row.selectorOptions = custom.field_values;
            }
            if (custom.required) {
                if (self.requaerdFields == nil) {
                    self.requaerdFields = [NSMutableArray new];
                }
                
                [self.requaerdFields addObject:custom.name];
                row.required = YES;
                NSString *messageRegex = [NSString stringWithFormat:@"Поле \"%@\" обязательно для заполнения", custom.title];
                [row addValidator:[XLFormRegexValidator formRegexValidatorWithMsg:messageRegex regex:@"^(?=).{1,140}$"]];
            }
            [section addFormRow:row];
        }
    }
    
    if (self.form.formSections.count >= 5) {
        [self.form removeFormSectionAtIndex:1];
    }
    [self.form addFormSection:section afterSection:self.form.formSections[0]];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0: return [self addViewForHeader:@"Категория и размещение" subTitle:@"Выберите категорию вашего объявления"];
            break;
        case 1: return [self addViewForHeader:@"Основные параметры" subTitle:@"Расскажите самое главное"];
            break;
        case 2: return [self addViewForHeader:@"Добавление фотографий" subTitle:@"Покажите товар лицом. Объявления даже с одной фотографией получает в 5 раз больше откликов!"];
            break;
        case 3: return [self addViewForHeader:@"Контактные данные" subTitle:@"Расскажите как с вами связаться"];
            break;
        default:
            break;
    }
    return nil;
}

- (NSMutableDictionary *)resultValue {
    
    NSArray *array = [self formValidationErrors];
    __block NSString *error;
    [array enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        XLFormValidationStatus * validationStatus = [[obj userInfo] objectForKey:XLValidationStatusErrorKey];
        if ([validationStatus.rowDescriptor.tag isEqualToString:@"e-mail"]) {
            UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[self.form indexPathOfFormRow:validationStatus.rowDescriptor]];
            [self animateCell:cell];
        }
        
        if ([validationStatus.rowDescriptor.tag isEqualToString:@"pass"]) {
            UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[self.form indexPathOfFormRow:validationStatus.rowDescriptor]];
            [self animateCell:cell];
        }
        
        for (NSString *tag in self.requaerdFields) {
            if ([validationStatus.rowDescriptor.tag isEqualToString:tag]) {
                UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[self.form indexPathOfFormRow:validationStatus.rowDescriptor]];
                [self animateCell:cell];
            }
        }
        error = validationStatus.msg;
    }];
    
    if (array.count == 0) {
        
        XLFormSectionDescriptor *section = self.form.formSections[1];
        NSMutableDictionary *result = [NSMutableDictionary dictionary];
        
        if (!section.isMultivaluedSection){
            for (XLFormRowDescriptor *row in section.formRows) {
                if (row.tag && ![row.tag isEqualToString:@""]){
                    if ([row.tag isEqualToString:@"price"] || [row.tag isEqualToString:@"title"] || [row.tag isEqualToString:@"currency"] || [row.tag isEqualToString:@"text"]) {
//                    if () {
//                        [self.dictCategoryRegion setObject:(row.value ?: [NSNull null]) forKey:row.tag];
                        [result setObject:(row.value ?: [NSNull null]) forKey:row.tag];
                    } else {
                        [result setObject:(row.value ?: [NSNull null]) forKey:[NSString stringWithFormat:@"custom_fields][%@][0", row.tag]];
                    }
                }
            }
        }
        else{
            NSMutableArray *multiValuedValuesArray = [NSMutableArray new];
            for (XLFormRowDescriptor *row in section.formRows) {
                if (row.value){
                    if ([row.tag isEqualToString:@"price"] || [row.tag isEqualToString:@"title"] || [row.tag isEqualToString:@"currency"] || [row.tag isEqualToString:@"text"]) {
//                    if ([row.tag isEqualToString:@"text"]) {
                        [result setObject:(row.value ?: [NSNull null]) forKey:row.tag];
//                        [self.dictCategoryRegion setObject:(row.value ?: [NSNull null]) forKey:row.tag];
                    } else {
                        [multiValuedValuesArray addObject:row.value];
                    }
                }
            }
            [result setObject:multiValuedValuesArray forKey:[NSString stringWithFormat:@"custom_fields][%@][0", section.multivaluedTag]];
        }
        
        XLFormSectionDescriptor *sectionCont = self.form.formSections[3];
        for (XLFormRowDescriptor *row in sectionCont.formRows) {
            if (row.tag && ![row.tag isEqualToString:@""]){
                [self.dictCategoryRegion setObject:(row.value ?: [NSNull null]) forKey:row.tag];
//                [result setObject:(row.value ?: [NSNull null]) forKey:row.tag];
            }
        }
        
        XLFormSectionDescriptor *sectionPhoto = self.form.formSections[2];
        for (XLFormRowDescriptor *row in sectionPhoto.formRows) {
            if (row.tag && ![row.tag isEqualToString:@""]){
                if (self.imageArray == nil) {
                    self.imageArray = [NSMutableArray new];
                }
                if (row.value != nil) {
                    [self.imageArray addObjectsFromArray:row.value];
                }
                
                //                [result setObject:(row.value ?: [NSNull null]) forKey:row.tag];
            }
        }
        
        return result;
    } else {
         [self alertWithMessages:error andTitle:@"Внимание!"];
    }
    return nil;
}

- (void)animateCell:(UITableViewCell *)cell {
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animation];
    animation.keyPath = @"position.x";
    animation.values =  @[ @0, @20, @-20, @10, @0];
    animation.keyTimes = @[@0, @(1 / 6.0), @(3 / 6.0), @(5 / 6.0), @1];
    animation.duration = 0.3;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    animation.additive = YES;
    
    [cell.layer addAnimation:animation forKey:@"shake"];
}

- (UIView *)addViewForHeader:(NSString *)title subTitle:(NSString *)subtitle {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 60)];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, self.tableView.frame.size.width, 20)];
    titleLabel.text = title;
    titleLabel.font = [UIFont fontWithName:@"AvenirNext-Medium" size:16.0];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [view addSubview:titleLabel];
    
    UILabel *detailLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 25, self.tableView.frame.size.width, 40)];
    detailLabel.text = subtitle;
    detailLabel.numberOfLines = 2;
    detailLabel.font = [UIFont fontWithName:@"AvenirNext-Medium" size:12.0];
    detailLabel.textColor = [UIColor grayColor];
    detailLabel.textAlignment = NSTextAlignmentCenter;
    [view addSubview:detailLabel];
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 4) {
        return 0;
    }
    return 60;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)alertWithMessages:(NSString *)messages andTitle:(NSString *)title {
    [[[UIAlertView alloc] initWithTitle:title
                                message:messages
                               delegate:self
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil, nil] show ];
}


@end
