//
//  RLGroupFieldsModel.m
//  realestateapp
//
//  Created by Юрий Дурнев on 30.07.15.
//  Copyright (c) 2015 irr. All rights reserved.
//

#import "RLGroupFieldsModel.h"

@implementation RLCustomFields

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"dictionaryField":@"dictionary",
             @"title":@"title",
             @"field_values":@"field_values",
             @"required":@"mandatory",
             @"dependentField":@"dependent",
             @"type":@"type",
             @"name":@"name"};
}

+ (NSArray *)deserializationCustomFields:(NSArray *)fields {
    NSError *error;
    
    NSArray *dictModel = [MTLJSONAdapter modelsOfClass:[RLCustomFields class] fromJSONArray:fields error:&error];
    if (error) {
        NSLog(@"Couldn't convert app infos JSON to %@ models: %@", [RLCustomFields class], error);
        return nil;
    }
    return dictModel;
}

@end

@implementation RLGroupFieldsModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"title":@"title",
             @"sortIndex":@"sort_index",
             @"customFields":@"custom_fields"};
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue error:(NSError *__autoreleasing *)error {
    self = [super initWithDictionary:dictionaryValue error:error];
    self.customFields = [RLCustomFields deserializationCustomFields:dictionaryValue[@"customFields"]];
    return self;
}

+ (NSArray *)deserializationGroupFields:(NSArray *)fields {
    NSError *error;
    
    NSArray *dictModel = [MTLJSONAdapter modelsOfClass:[RLGroupFieldsModel class] fromJSONArray:fields error:&error];
    if (error) {
        NSLog(@"Couldn't convert app infos JSON to %@ models: %@", [RLGroupFieldsModel class], error);
        return nil;
    }
    return dictModel;
}

@end
